## 0.6.1
- Add BpriverYaml.parseYamlDocumentAsT
    - YamlList, YamlMap, 以外にも parse できるように.
- Delete BpriverYaml.parseYamlDocumentAsYamlList
- Delete BpriverYaml.parseYamlDocumentAsYamlMap

## 0.6.0
- BugFix AggregationPattern.toString
    - properties が空の場合に range error が発生.
- Edit BpriverYaml.toYamlString
    - 文字列を出力する際に 可能であれば quote で囲まない状態で出力するように調整.
- BpriverYaml library と統合.
    - bpriver origin を更新した際に、bpriver_yaml も更新する必要があり、それが手間なため.
- Delete BpriverOriginException
    - それぞれの library 内で管理するように変更
- Delete BpriverOriginError
    - それぞれの library 内で管理するように変更

## 0.5.2
- Edit RosterPattern.toJson
    - toJson で返す記述を書き忘れ.

## 0.5.1
- Edit AggregationPattern.inactivate and IterablePattern.inactivate
    - AggregationPattern の property に IterablePattern があるとバグった.
    - iterable.map() による処理を for に変更.
- Add InactivateSignature
- Delete ValuePattern.inactivate
    - 呼び出し先の ValuePattern の class で InactivateSignature を実装すればよい.

## 0.5.0
- Edit EnumPattern.toJson
    - fromValue constructor を流用できるように 出力結果を value の値に変更.
- Add test IterablePattern.toJson
    - values の値が class でも primitive でも同じ出力になるかの確認.

## 0.4.2
- Edit toJson
    - class に Generics がある場合 その Generics の型も出力されていたのを class name のみを出力するように変更.

## 0.4.1
- Edit AggregationPattern.toJson
    - value が toString されていたためこれをなくした.
    - toString をしてしまっているので int 型なども String 型になってしまっていた.

## 0.4.0
- Add PropertiesSignature
    - toMap の代わり. toMap は method だったが getter として定義する.
- Delete DesignPatternToMapSignature
    - properties を使う.
- Edit toJson
- Delete Log.monitorAll
- Delete Log.debugAll
- Edit Log.monitor
    - Map を受け取るようにする
    - 要は moniorAll を monitor に統合した
- Edit Log.debug
    - monitor と同様.

## 0.3.4
- Edit History.fromResult
    - resultType に入る値を wrapped された instance の型に変更.

## 0.3.3
- yaml 関連を bpriver yaml へすべて移行

## 0.3.2
- bpriver_origin_extension と統合
    - Add IterablePatternInternalFactory mixin
    - Add RosterPatternPut mixin
    - Add RosterPatternPutAll mixin

## 0.3.1
- History.toMoldedYamlString
    - output molded yaml string.
- History.toYamlString
    - output not molded yaml string.
- History._toEncodable
    - 上記 2つのmethod に必要.

## 0.3.0
- Update dart sdk
    3.6.0

## 0.1.1
- Add test\design_pattern\iterable\roster\roster_pattern_test.dart
    - generative constructor test.

## 0.1.0
- Major changes
- Changed license from BSD to AGPL