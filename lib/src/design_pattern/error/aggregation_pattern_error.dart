// Copyright (C) 2025, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template AggregationPatternError}
/// {@endtemplate}
/// * [AggregationPatternErrorA]
/// {@macro AggregationPatternErrorA}
/// * [AggregationPatternErrorB]
/// {@macro AggregationPatternErrorB}
/// * [AggregationPatternErrorC]
/// {@macro AggregationPatternErrorC}
/// * [AggregationPatternErrorD]
/// {@macro AggregationPatternErrorD}
/// * [AggregationPatternErrorE]
/// {@macro AggregationPatternErrorE}
/// * [AggregationPatternErrorF]
/// {@macro AggregationPatternErrorF}
/// * [AggregationPatternErrorG]
/// {@macro AggregationPatternErrorG}
sealed class AggregationPatternError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template BPRiverOriginErrorA}
/// old BPRiverOriginErrorA.<br>
/// [] operator で無効な key を指定.
/// {@endtemplate}
final class AggregationPatternErrorA
    extends
        AggregationPatternError
{

    static const LOGGER_RESULT_MESSAGE = [
        '[] operator で無効な key を指定.'
        // '入力した key: $invalidKey',
        // '有効な key の一覧: $validKeys',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro AggregationPatternErrorA}
    AggregationPatternErrorA();

}
