// Copyright (C) 2025, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template EnumPatternError}
/// {@endtemplate}
/// * [EnumPatternErrorA]
/// {@macro EnumPatternErrorA}
/// * [EnumPatternErrorB]
/// {@macro EnumPatternErrorB}
/// * [EnumPatternErrorC]
/// {@macro EnumPatternErrorC}
/// * [EnumPatternErrorD]
/// {@macro EnumPatternErrorD}
/// * [EnumPatternErrorE]
/// {@macro EnumPatternErrorE}
/// * [EnumPatternErrorF]
/// {@macro EnumPatternErrorF}
/// * [EnumPatternErrorG]
/// {@macro EnumPatternErrorG}
sealed class EnumPatternError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template EnumPatternErrorA}
/// old BPRiverOriginErrorD.<br>
/// fromType constructor で無効な Type を指定.<br>
/// 考えられる可能性.
/// 1. final class type ではない abstract class type を指定(呼び出し側の不備).
/// 2. 分岐先の未実装による分岐漏れ(呼び出し元の不備).
/// {@endtemplate}
final class EnumPatternErrorA
    extends
        EnumPatternError
{

    static const LOGGER_RESULT_MESSAGE = [
        '[EnumPattern] の fromType constructor で無効な Type を指定.',
        '考えられる可能性.',
        '1. final class type ではない abstract class type を指定(呼び出し側の不備).',
        '2. 分岐先の未実装による分岐漏れ(呼び出し元の不備).',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro EnumPatternErrorA}
    EnumPatternErrorA();

}
