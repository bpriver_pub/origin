// Copyright (C) 2025, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template IterablePatternIteratorError}
/// {@endtemplate}
/// * [IterablePatternIteratorErrorA]
/// {@macro IterablePatternIteratorErrorA}
/// * [IterablePatternIteratorErrorB]
/// {@macro IterablePatternIteratorErrorB}
/// * [IterablePatternIteratorErrorC]
/// {@macro IterablePatternIteratorErrorC}
/// * [IterablePatternIteratorErrorD]
/// {@macro IterablePatternIteratorErrorD}
/// * [IterablePatternIteratorErrorE]
/// {@macro IterablePatternIteratorErrorE}
/// * [IterablePatternIteratorErrorF]
/// {@macro IterablePatternIteratorErrorF}
/// * [IterablePatternIteratorErrorG]
/// {@macro IterablePatternIteratorErrorG}
sealed class IterablePatternIteratorError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template IterablePatternIteratorErrorA}
/// old BPRiverOriginErrorB.<br>
/// _current が null.
/// {@endtemplate}
final class IterablePatternIteratorErrorA
    extends
        IterablePatternIteratorError
{

    static const LOGGER_RESULT_MESSAGE = [
        '_current が null.'
    ];

    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro IterablePatternIteratorErrorA}
    IterablePatternIteratorErrorA();

}
