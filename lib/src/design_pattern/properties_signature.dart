// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template PropertiesSignature}
/// 
/// inactivate(), = operator, [] operator, など に依存されている.<br>
/// 
/// 定義例：
/// 
/// Map<String, Object> get properties {
///     return {
///         'stateA': stateA,
///         'stateB': stateB,
///         'stateC': stateC,
///     };
/// }
/// 
/// {@endtemplate}
abstract class PropertiesSignature
{

    /// {@macro PropertiesSignature}
    Map<String, Object> get properties;

}
