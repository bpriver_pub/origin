// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

mixin ListPatternPrivateMemberTester<
        VT extends Object
    >
    on
        ListPattern<VT>
{
    
    Object toNestedStructureMapEntryA(AggregationPattern element) => _toNestedStructureMapEntryA(element);

    Map<String, Object> toNestedStructureMapEntryB(IterablePattern<AggregationPattern> values) => _toNestedStructureMapEntryB(values);

    Object toNestedStructureMapEntryC() => _toNestedStructureMapEntryC();

}