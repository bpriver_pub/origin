// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

mixin ListPattern<
        VT extends Object
    >
    implements
        IterablePattern<VT>
        ,InactivateSignature
{

    @override
    void checkComputation() {}

    @override
    Iterator<VT> get iterator => IterablePatternIterator<VT>(values);

    @override
    int get hashCode {
        var result = 17;
        for (var value in values) {
            result = 37 * result + value.hashCode;
        }
        return result;
    }

    @override
    Set<VT> toSet() {
        Set<VT> result = {};
        for (final e in values) {
            if (result.contains(e)) continue;
            result.add(e);
        }
        return result;
    }

    // 20230807 時点 
    //     == operator で利用していた. 
    //     順番も考慮した specific になったため contatisAll は使用しなくなった.
    //     IterablePattern に実装する必要はなくなったが特に取り除く緊急性もないため放置.
    bool containsAll(Iterable<VT> models) {
        for (var model in models) {
            if(!contains(model)) return false;
        }
        return true;
    }

    // 20230807時点から 順番まで考慮するように変更した. 順番が異なると false を返す.
    @override
    bool operator ==(Object other) {
        if(other.runtimeType != runtimeType) return false;
        other as IterablePattern<VT>;
        if(other.length != length) return false; // [A,A,A] [A,A] => length も比較が必要。
        final list = List.generate(length, (index) => index);
        for (final e in list) {
            if (values.elementAt(e) != other.elementAt(e)) return false;
        }
        return true;
    }

    @override
    String toString() {
        /// toList() で、List に変換しないと（Iterable だと）、
        /// 長い String が「...」で途中を丸められてしまう。
        /// https://api.dart.dev/stable/2.9.0/dart-core/Iterable/toString.html
        return map((model)=>model.toString()).toList().toString();
    }

    @override
    Map<String, Object> get properties {
        return {
            'values': values,
        };
    }

    @override
    Map<String, Object> toJson() {

        const lessThan = '<';
        final type = runtimeType.toString();
        late final String head;

        final index = type.indexOf(lessThan);
        if (index == -1) {
            head = type;
        } else {
            head = type.substring(0, index);
        }

        final List<Object> list = List.generate(length, (index) {
            final value = values.elementAt(index);
            if (value is ToJsonSignature) return value.toJson();
            return value;
        });
        return {
            head: {
                'values': list,
            },
        };
    }

    @override
    Iterable<Object> inactivate() {

        if (values is Iterable<InactivateSignature>) return List.generate(length, (index) {
            return (elementAt(index) as InactivateSignature).inactivate();
        });

        return values;

    }

    Object _toNestedStructureMapEntryA(AggregationPattern element) {
        switch (element) {
            case AggregationPatternToShorthandSignature():
                return element.toShorthand();
            case _:
                return element.toNestedStructureMapEntry().value;
        }
    }

    Map<String, Object> _toNestedStructureMapEntryB(Iterable<AggregationPattern> values) {
        
        final Map<String, Object> map = {};
        int index = 0;
        
        for (final e in values) {
            final key = '${e.runtimeType}[${index}]';
            final value = _toNestedStructureMapEntryA(e);
            map.addAll({key: value});
            index = index + 1;
        }

        return map;

    }

    Object _toNestedStructureMapEntryC() {
        switch (values) {
            case Iterable<AggregationPattern>():
                return _toNestedStructureMapEntryB(values as Iterable<AggregationPattern>);
            case Iterable<ValuePattern>():
                return join(', ');
            case _:
                return toString();
        }
    }

    @override
    MapEntry<String, Object> toNestedStructureMapEntry() {

        final key = '${runtimeType}';
        final value = _toNestedStructureMapEntryC();

        return MapEntry(key, value);

    }

    @override
    Map<String, Object> toNestedStructureMap() => Map.fromEntries([toNestedStructureMapEntry()]);

    Danger<VT, IterablePatternExceptionABC> getByIndex(int index) {

        final log = Log(classLocation: runtimeType, functionLocation: 'getByIndex');

        if (isEmpty) return Failure(IterablePatternExceptionC(), log);

        if (index < 0) return Failure(IterablePatternExceptionA(), log.monitor({'index': index}));

        if (length < index + 1) return Failure(IterablePatternExceptionB(), log.monitor({
            'index': index,
            'length': length,
        }));

        final result = elementAt(index);

        return Success(result, log);

    }

    // IterableMixin の実装
    @override
    bool any(bool Function(VT element) test) => values.any(test);

    @override
    Iterable<R> cast<R>() => values.cast<R>();

    @override
    bool contains(Object? element) => values.contains(element);

    @override
    VT elementAt(int index) => values.elementAt(index);

    @override
    bool every(bool Function(VT element) test) => values.every(test);

    @override
    Iterable<T> expand<T>(Iterable<T> Function(VT element) toElements) => values.expand(toElements);

    @override
    VT get first => values.first;

    @override
    VT firstWhere(bool Function(VT element) test, {VT Function()? orElse}) => values.firstWhere(test, orElse: orElse);

    @override
    T fold<T>(T initialValue, T Function(T previousValue, VT element) combine) => values.fold(initialValue, combine);

    @override
    Iterable<VT> followedBy(Iterable<VT> other) => values.followedBy(other);

    @override
    void forEach(void Function(VT element) action) => values.forEach(action);

    @override
    bool get isEmpty => values.isEmpty;

    @override
    bool get isNotEmpty => values.isNotEmpty;

    @override
    String join([String separator = ""]) => values.join(separator);

    @override
    VT get last => values.last;

    @override
    VT lastWhere(bool Function(VT element) test, {VT Function()? orElse}) => values.lastWhere(test, orElse: orElse);

    @override
    int get length => values.length;

    @override
    Iterable<T> map<T>(T Function(VT e) toElement) => values.map(toElement);

    @override
    VT reduce(VT Function(VT value, VT element) combine) => values.reduce(combine);

    @override
    VT get single => values.single;

    @override
    VT singleWhere(bool Function(VT element) test, {VT Function()? orElse}) => values.singleWhere(test, orElse: orElse);

    @override
    Iterable<VT> skip(int count) => values.skip(count);

    @override
    Iterable<VT> skipWhile(bool Function(VT value) test) => values.skipWhile(test);

    @override
    Iterable<VT> take(int count) => values.take(count);

    @override
    Iterable<VT> takeWhile(bool Function(VT value) test) => values.takeWhile(test);

    @override
    List<VT> toList({bool growable = true}) => values.toList(growable: growable);

    @override
    Iterable<VT> where(bool Function(VT element) test) => values.where(test);

    @override
    Iterable<T> whereType<T>() => values.whereType<T>();

}