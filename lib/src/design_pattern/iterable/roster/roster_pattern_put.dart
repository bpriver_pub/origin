// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template RosterPatternPut}
/// 
/// PK...primary key
/// VT...vlaue type
/// RT... return type
/// 
/// value を追加 もしくは 上書き する.<br>
/// 
/// [put] によって [RosterPatternExceptionA] は発生しない.<br>
/// なので 呼び出し元は これを throw out する必要がある.<br>
/// 
/// {@endtemplate}
mixin RosterPatternPut<
        PK extends Object
        ,VT extends EntityPattern<PK>
        ,RT extends Result<Iterable<VT>>
    >
    on
        RosterPattern<PK, VT>
        ,IterablePatternInternalFactory<VT,RT>
{

    /// {@macro RosterPatternPut}
    RT put(VT value) {

        final List<VT> list = [];
        bool flag = true;

        for (final e in values) {
            
            if (e.primaryKey == value.primaryKey) {
                list.add(value);
                flag = false;
            } else {
                list.add(e);
            }

        }

        // 同じ primary key をもったものが存在しない場合 最後に value を追加する.
        if (flag) list.add(value);

        return internalFactory(list);

    }

}
