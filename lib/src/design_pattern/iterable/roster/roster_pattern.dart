// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

// roster pattern のように 保存に primary key を利用している場合 put(create, update), delete, get, の 3つでよい.
// これは create, update, 両方とも primary key の重複が無いかを必ず確認しなければならないため put という 1つの命令にまとめても オーバーヘッドがほとんどない.
//      list のような 重複が許可されたものは create と update を分けた方が 処理のオーバーヘッドを少なくできる.
//      create の処理時に list 内での重複などをいちいち調べる必要が無いため 加える処理だけで済む.
// 参考
//  leveldb/doc/index.md at main · google/leveldb · GitHub: "https://github.com/google/leveldb/blob/main/doc/index.md"

/// {@template ListPattern}
/// 
/// throwable [RosterPatternExceptionA].
/// 
/// roster ... 名簿
/// 
/// identifier を軸に管理するための iterable.
/// primary key もつ model(entity) を一意に管理する iterable.
///     id でなくてもよい.
/// 
/// 重複した primary key があると error.
///     重複を検知するために primary key は = を override し 利用できるようにしておく必要がある.
/// 
/// PK...PrimaryKey
/// VT・・・ValueType
/// 
/// {@endtemplate}
mixin RosterPattern<
        PK extends Object
        ,VT extends EntityPattern<PK>
    >
    implements
        IterablePattern<VT>
        ,InactivateSignature
{

    static void checkRosterPatternExceptionA<PK extends Object, VT extends EntityPattern<PK>>(Iterable<EntityPattern> values) {

        final log = Log(classLocation: RosterPattern<PK, VT>, functionLocation: 'checkRosterPatternExceptionA');

        final primaryKeyList = List.generate(values.length, (index) => values.elementAt(index).primaryKey);

        if(primaryKeyList.toSet().length == primaryKeyList.length) return;

        throw Failure(RosterPatternExceptionA(), log.monitor({
            'primary keys': primaryKeyList
        }));

    }

    @override
    void checkComputation() {
        checkRosterPatternExceptionA<PK, VT>(values);
    }

    /// {@template RosterPattern.getByPrimaryKey}
    /// 
    /// {@endtemplate}
    Danger<VT, RosterPatternExceptionB> getByPrimaryKey(PK primaryKey) {

        final log = Log(classLocation: RosterPattern, functionLocation: 'getByPrimaryKey');
        
        return Danger.tryAndCatch(
            () {
                return values.singleWhere(
                    (element) => element.primaryKey == primaryKey,
                    orElse: () {
                        final primaryKeys = List.generate(length, (index) => elementAt(index).primaryKey);
                        throw Failure(RosterPatternExceptionB(), log.monitor({
                            'primary key': primaryKey,
                            'primary keys': primaryKeys,
                        }));
                    },
                );
            },
            log,
        );
        
    }

    List<PK> get primaryKeys {
        return List.generate(length, (index) => elementAt(index).primaryKey);
    }

    /// {@template RosterPattern.containsPrimaryKey}
    /// 
    /// {@endtemplate}
    Safety<bool> containsPrimaryKey(PK primaryKey) {

        final log = Log(classLocation: runtimeType, functionLocation: 'containsPrimaryKey');

        // values が空の場合 必ず false が返る.
        final anyResult = any((element) => element.primaryKey == primaryKey);

        return Safety(anyResult, log);

    }

    /// {@template RosterPattern.containsPrimaryKeyAll}
    /// [primaryKeys] がすべて存在する場合 true を返す.<br>
    /// [primaryKeys] が空の場合 true を返す.<br>
    /// [values] が空の場合 false を返す.<br>
    /// [primaryKeys] と [values] が空の場合 true を返す.<br>
    /// {@endtemplate}
    Safety<bool> containsPrimaryKeyAll(Iterable<PK> primaryKeys) {

        final log = Log(classLocation: runtimeType, functionLocation: 'containsPrimaryKeyAll');

        for (final e in primaryKeys) {

            // values が空の場合 必ず false が返る.
            final anyResult = any((element) => element.primaryKey == e);
            if (!anyResult) return Safety(false, log);

        }

        return Safety(true, log);

    }

    // copied from ListPattern.
    @override
    Iterator<VT> get iterator => IterablePatternIterator<VT>(values);

    // copied from ListPattern.
    @override
    int get hashCode {
        var result = 17;
        for (var value in values) {
            result = 37 * result + value.hashCode;
        }
        return result;
    }

    // copied from ListPattern.
    @override
    Set<VT> toSet() {
        Set<VT> result = {};
        for (final e in values) {
            if (result.contains(e)) continue;
            result.add(e);
        }
        return result;
    }

    // copied from ListPattern.
    // 20230807 時点 
    //     == operator で利用していた. 
    //     順番も考慮した specific になったため contatisAll は使用しなくなった.
    //     IterablePattern に実装する必要はなくなったが特に取り除く緊急性もないため放置.
    bool containsAll(Iterable<VT> models) {
        for (var model in models) {
            if(!contains(model)) return false;
        }
        return true;
    }

    // copied from ListPattern.
    // 順番まで考慮している. 異なると false を返す.
    @override
    bool operator ==(Object other) {
        if(other.runtimeType != runtimeType) return false;
        other as IterablePattern<VT>;
        if(other.length != length) return false; // [A,A,A] [A,A] => length も比較が必要。
        final list = List.generate(length, (index) => index);
        for (final e in list) {
            if (values.elementAt(e) != other.elementAt(e)) return false;
        }
        return true;
    }

    // copied from ListPattern.
    @override
    String toString() {
        /// toList() で、List に変換しないと（Iterable だと）、
        /// 長い String が「...」で途中を丸められてしまう。
        /// https://api.dart.dev/stable/2.9.0/dart-core/Iterable/toString.html
        return map((model)=>model.toString()).toList().toString();
    }

    // copied from ListPattern.
    @override
    Map<String, Object> get properties {
        return {
            'values': values,
        };
    }

    @override
    Map<String, Object> toJson() {

        const lessThan = '<';
        final type = runtimeType.toString();
        late final String head;

        final index = type.indexOf(lessThan);
        if (index == -1) {
            head = type;
        } else {
            head = type.substring(0, index);
        }

        final List<Map<String, Object>> list = List.generate(length, (index) {
            final value = values.elementAt(index);
            return value.toJson();
        });
        return {
            head: {
                'values': list,
            },
        };
    }

    @override
    Iterable<Object> inactivate() {

        return List.generate(length, (index) {
            return (elementAt(index) as InactivateSignature).inactivate();
        });

    }

    // copied from ListPattern.
    Object _toNestedStructureMapEntryA(AggregationPattern element) {
        switch (element) {
            case AggregationPatternToShorthandSignature():
                return element.toShorthand();
            case _:
                return element.toNestedStructureMapEntry().value;
        }
    }

    // copied from ListPattern.
    Map<String, Object> _toNestedStructureMapEntryB(Iterable<AggregationPattern> values) {
        
        final Map<String, Object> map = {};
        int index = 0;
        
        for (final e in values) {
            final key = '${e.runtimeType}[${index}]';
            final value = _toNestedStructureMapEntryA(e);
            map.addAll({key: value});
            index = index + 1;
        }

        return map;

    }

    // copied from ListPattern.
    Object _toNestedStructureMapEntryC() {
        switch (values) {
            case Iterable<AggregationPattern>():
                return _toNestedStructureMapEntryB(values as Iterable<AggregationPattern>);
            // case Iterable<ValuePattern>():
            //     return join(', ');
            // case _:
            //     return toString();
        }
    }

    @override
    MapEntry<String, Object> toNestedStructureMapEntry() {

        final key = '${runtimeType}';
        final value = _toNestedStructureMapEntryC();

        return MapEntry(key, value);

    }

    // copied from ListPattern.
    @override
    Map<String, Object> toNestedStructureMap() => Map.fromEntries([toNestedStructureMapEntry()]);

    // copied from ListPattern.
    Danger<VT, IterablePatternExceptionABC> getByIndex(int index) {

        final log = Log(classLocation: runtimeType, functionLocation: 'getByIndex');

        if (isEmpty) return Failure(IterablePatternExceptionC(), log);

        if (index < 0) return Failure(IterablePatternExceptionA(), log.monitor({'index': index}));

        if (length < index + 1) return Failure(IterablePatternExceptionB(), log.monitor({
            'index': index,
            'length': length,
        }));

        final result = elementAt(index);

        return Success(result, log);

    }

    // copied from ListPattern.
    // IterableMixin の実装
    @override
    bool any(bool Function(VT element) test) => values.any(test);

    @override
    Iterable<R> cast<R>() => values.cast<R>();

    @override
    bool contains(Object? element) => values.contains(element);

    @override
    VT elementAt(int index) => values.elementAt(index);

    @override
    bool every(bool Function(VT element) test) => values.every(test);

    @override
    Iterable<T> expand<T>(Iterable<T> Function(VT element) toElements) => values.expand(toElements);

    @override
    VT get first => values.first;

    @override
    VT firstWhere(bool Function(VT element) test, {VT Function()? orElse}) => values.firstWhere(test, orElse: orElse);

    @override
    T fold<T>(T initialValue, T Function(T previousValue, VT element) combine) => values.fold(initialValue, combine);

    @override
    Iterable<VT> followedBy(Iterable<VT> other) => values.followedBy(other);

    @override
    void forEach(void Function(VT element) action) => values.forEach(action);

    @override
    bool get isEmpty => values.isEmpty;

    @override
    bool get isNotEmpty => values.isNotEmpty;

    @override
    String join([String separator = ""]) => values.join(separator);

    @override
    VT get last => values.last;

    @override
    VT lastWhere(bool Function(VT element) test, {VT Function()? orElse}) => values.lastWhere(test, orElse: orElse);

    @override
    int get length => values.length;

    @override
    Iterable<T> map<T>(T Function(VT e) toElement) => values.map(toElement);

    @override
    VT reduce(VT Function(VT value, VT element) combine) => values.reduce(combine);

    @override
    VT get single => values.single;

    @override
    VT singleWhere(bool Function(VT element) test, {VT Function()? orElse}) => values.singleWhere(test, orElse: orElse);

    @override
    Iterable<VT> skip(int count) => values.skip(count);

    @override
    Iterable<VT> skipWhile(bool Function(VT value) test) => values.skipWhile(test);

    @override
    Iterable<VT> take(int count) => values.take(count);

    @override
    Iterable<VT> takeWhile(bool Function(VT value) test) => values.takeWhile(test);

    @override
    List<VT> toList({bool growable = true}) => values.toList(growable: growable);

    @override
    Iterable<VT> where(bool Function(VT element) test) => values.where(test);

    @override
    Iterable<T> whereType<T>() => values.whereType<T>();

}