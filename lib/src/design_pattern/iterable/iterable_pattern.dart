// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

// IterablePattern を直接 with することは無い.
// ListPattern, SetPattern, RosterPattern, を with する.
mixin IterablePattern<
        VT extends Object
    >
    on
        Computation
    implements
        IterableMixin<VT>
        ,PropertiesSignature
        ,ToJsonSignature
        ,DesignPatternToNestedStructureMapEntrySignature
        ,DesignPatternToNestedStructureMapSignature
{

    Iterable<VT> get values;

    // == operator で利用している.
    bool containsAll(Iterable<VT> models);

    Iterable<Object> inactivate();

    // elementAt と基本的には同じだが、あちらは error が throw されてしまう.<br>
    // そのため 投げられた error に log の情報は無いし、try catch による handling が面倒である.<br>
    // なので ここでは exception を返し error とするか exception とするかは 呼び出し側に委ねる.<br>
    Danger<VT, IterablePatternExceptionABC> getByIndex(int index);

}
