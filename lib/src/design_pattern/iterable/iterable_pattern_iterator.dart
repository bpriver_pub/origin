// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

class IterablePatternIterator<T> implements Iterator<T> {
    final List<T> _values;
    final int _length; // Cache array length for faster access.
    int _position;
    T? _current;
    IterablePatternIterator(Iterable<T> array)
        : _values = array.toList(),
            _position = -1,
            _length = array.length,
            // _length が 0 の場合 _values.first で error.
            _current = array.length == 0 ? null : array.first;

    @override
    bool moveNext() {
        // _length が 0 の場合 _values.first で error.
        if(_length == 0) return false;

        var nextPosition = _position + 1;
        if (nextPosition < _length) {
        _current = _values[nextPosition];
        _position = nextPosition;
        return true;
        }
        _current = _values.first;
        _position = _length;
        return false;
    }

    @override
    T get current {
        if(_current == null) Panic(IterablePatternIteratorErrorA(), Log(classLocation: IterablePatternIterator, functionLocation: 'current'));
        return _current!;
    }
  
}
