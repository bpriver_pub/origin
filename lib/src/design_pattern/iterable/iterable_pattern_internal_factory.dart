// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template IterablePatternInternalFactory}
/// 
/// IterablePattern の 内部用の factory method.
/// 
/// VT...ValuesType
/// RT...ReturnType
/// 
/// {@endtemplate}
mixin IterablePatternInternalFactory<
        VT extends Object
        ,RT extends Result<Iterable<VT>>
    >
    on
        IterablePattern<VT>
{

    RT internalFactory(Iterable<VT> values);

}
