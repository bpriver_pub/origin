// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

mixin DesignPatternToNestedStructureMapEntrySignature
{

    /// {@template DesignPatternToNestedStructureMapEntrySignature.toNestedStructureMapEntry}
    /// 
    /// 1. Map ではなく MapEntry である必要性
    /// まず 前提として key 名には 型 を使う.
    /// しかし それだと同じ key 名になってしまうことがあり そうなると 上書きされてしまい これを防ぐ必要がある.
    /// root の key 名は 型 のみだが
    /// nest された key 名は 型 + 接頭辞 or 接尾辞 とする.
    /// nest された型の key 名を編集するにあたり MapEntry がやりやすい.
    /// 
    /// 2. それぞれの値の返し方
    ///     1. AggregationPattern で toShorthand を実装
    ///         toShorthand の処理は定義側が自由に定義する.
    ///         だいたいは csv のようなコンマ区切りの値をまとめた1行表示を想定している.
    ///         多重入れ子構造の class の場合一番深い class にこの toShorthand が定義されていることを期待している.
    ///         一番深い class は すべての property が ValuePattern であるはずなので.
    ///     2. AggregationPattern で toShorthand を未実装
    ///         これは 階層の上の部分を想定しており これらは自分が property としてもつ AggregationPattern.toNestedStructureMapEntry.value, ValuePattern.value などを呼ぶことを想定.
    ///     3. IterablePattern<ValuePattern>
    ///         値をカンマ区切りで表示する
    ///     4. IterablePattern<AggregationPattern>
    ///         それぞれを識字で接尾辞をつけ toNestedStructureMapEntry.value を呼び出し列挙する.
    ///     5. IterablePattern<Object>
    ///         基本的にこのような使い方は想定していない.
    ///         しかし いちいち error としてもめんどうなので toString() を表示する.
    ///     6. MapPettern
    ///         考慮事項が多く また 必要性も今のところないので 実装していない.
    ///         必要であれば都度 [DesignPatternToNestedStructureMapEntrySignature] を実装させる方針.
    /// {@endtemplate}
    MapEntry<String, Object> toNestedStructureMapEntry();

}
