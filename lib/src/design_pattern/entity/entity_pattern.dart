// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template EntityPattern}
/// PK...PrimaryKey
/// {@endtemplate}
mixin EntityPattern<
        // PK を設定することで RosterPattern.getByPrimarykey を実装できる.
        PK extends Object
    >
    on
        AggregationPattern
{

    // roster pattern で 一意であることを管理するために参照される値.
    // aggregation pattern でのみ 利用される.
    // value pattern は value しか値を持たないので set にすれば 一意であることを保証できる.
    PK get primaryKey;

}