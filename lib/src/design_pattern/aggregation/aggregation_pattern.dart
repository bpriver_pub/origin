// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template AggregationPattern}
/// １．toPrimitive() を override する => hashCode, ==, [], toString(), も自動的に更新される。
/// {@endtemplate}
mixin AggregationPattern
    implements
        PropertiesSignature
        ,ToJsonSignature
        ,DesignPatternToNestedStructureMapEntrySignature
        ,DesignPatternToNestedStructureMapSignature
        ,InactivateSignature
{

    /// properties に依存.
    @override
    int get hashCode {
        var result = 17;
        for (var property in properties.values ) {
            result = 37 * result + property.hashCode;
        }
        return result;
    }

    /// instance properties の toString() をそれぞれ比較し合う。
    /// properties, [] operator に依存.
    @override
    bool operator ==(Object other){
        if( other.runtimeType != runtimeType ) return false;
        final typeChecked = other as AggregationPattern;
        
        for (var key in properties.keys) {
            if(typeChecked[key].toString() != this[key].toString()) return false;
        }

        return true;
    }

    /// properties から、operator == の内容を自動更新できるようにするために定義している.
    Object operator [](String key) {
        if(properties.containsKey(key)) return properties[key] as Object;
        // for (var item in toMap().keys) {
        //     if(key == item) return toJson()[key] as Object;
        // }
        Result.panic(AggregationPatternErrorA(), Log(classLocation: AggregationPattern, functionLocation: '[] operator', monitor: {
            'input key': key,
            'valid keys': properties.keys,
        }));
    }

    /// properties に依存.
    @override
    String toString(){
        
        // properties が空の場合 '{}' を返す.
        // また 以降の trimed.substring で minus の 値が引数に実行されると RangedError が発生してしまう.
        if (properties.isEmpty) return '{}';

        var content = '';

        for (final i in properties.entries) {
            content = content + '${i.key}: ${i.value}, ';
        }
        
        var trimed = content.trim();
        var result = trimed.substring(0, trimed.length -1);
        return '{$result}';

    }

    // properties に依存.
    @override
    Map<String, Object> toJson() {

        const lessThan = '<';
        final type = runtimeType.toString();
        late final String head;

        final index = type.indexOf(lessThan);
        if (index == -1) {
            head = type;
        } else {
            head = type.substring(0, index);
        }
        
        final edited = properties.entries.map((e) {
            final value = e.value;
            if (value is ToJsonSignature) return MapEntry(e.key, value.toJson());
            return MapEntry(e.key, value);
        });

        return {
            head: Map.fromEntries(edited),
        };

    }

    @override
    Map<String, Object> inactivate() {

        final entries = List.generate(properties.length, (index) {

            final entry = properties.entries.elementAt(index);
            final value = entry.value;

            if (value is InactivateSignature) return MapEntry(entry.key, value.inactivate());

            return MapEntry(entry.key, value);

        });

        return Map.fromEntries(entries);

    }

    Object _toNestedStructureMapEntryA(Object entryValue) {
        switch (entryValue) {
            case AggregationPatternToShorthandSignature():
                return entryValue.toShorthand();
            // IterablePattern<VT> はここに分類される.
            case DesignPatternToNestedStructureMapEntrySignature():
                return entryValue.toNestedStructureMapEntry().value;
            case ValuePattern():
                return entryValue.value;
            // MapPettern はめんどくさいので必要があればその都度実装させる.
            case _:
                // test がめんどくさいので どれにも当てはまらない場合 toString を返す.
                return entryValue.toString();
        }
    }

    Map<String, Object> _toNestedStructureMapEntryB(AggregationPattern self) {
        final Map<String, Object> map = {};
        final entries = self.properties.entries;
        for (final entry in entries) {
            final entryKey = entry.key;
            final entryValue = entry.value;
            final key = '${entryValue.runtimeType}(${entryKey})';
            final value = _toNestedStructureMapEntryA(entryValue);
            map.addAll({key: value});
        }
        return map;
    }

    Object _toNestedStructureMapEntryC(AggregationPattern self) {
        switch (self) {
            case AggregationPatternToShorthandSignature():
                return self.toShorthand();
            case _:
                return _toNestedStructureMapEntryB(self);
        }
    }

    @override
    MapEntry<String, Object> toNestedStructureMapEntry() {

        final value = _toNestedStructureMapEntryC(this);

        return MapEntry('${runtimeType}', value);

    }

    @override
    Map<String, Object> toNestedStructureMap() => Map.fromEntries([toNestedStructureMapEntry()]);

}

mixin AggregationPatternTester
    on
        AggregationPattern
{
    
    Object toNestedStructureMapEntryA(Object entryValue) => _toNestedStructureMapEntryA(entryValue);
    
    Map<String, Object> toNestedStructureMapEntryB(AggregationPattern self) => _toNestedStructureMapEntryB(self);

    Object toNestedStructureMapEntryC(AggregationPattern self) => _toNestedStructureMapEntryC(self);

}
