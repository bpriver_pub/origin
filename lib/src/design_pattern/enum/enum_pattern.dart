// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template EnumPattern}
/// 
/// 1つの固定値のみを持つ pattern.
/// 
/// value という名前の compile time constant(コンパイル時定数, 修飾子は const) を 1つ だけ持つ class.
/// 
/// static access 用に VALUE という名前の property を定義する.
/// 
/// instance access 用に [value] という名前の property を定義する.
///     この [value] は static access 用の VALUE を参照する.
/// 
/// runtime validation が不要な class なので実行速度が速い.
/// 
/// 値は変化しないので inactivate() は定義しない.
/// 
/// hashCode もしくは == operator を override すると その onstance は constant set として利用できない.
///     なので hashCode および == operator は override しない.
///     実際に比較などするときは == operator を使わず is を利用するので問題ない.
///     しかし instance を == で比較できない.
/// 
/// static property(static な property は override できないため ここで理論だけ提示)
///     FINAL_TYPE_STRING_SET
///         final class の type の文字列の set.
///         Type 型のままだと json に 変換できないため [BPRiverOriginErrorD] が発生したときに log に type の set を保持するときに利用.
///         -> これは yaml_writter の仕様であり 解決策があるので大丈夫になった(20240613)
///             つまり FINAL_TYPE_STRING_SET は不要になった.
///     FINAL_TYPE_SET
///         error 時の log に渡すのは FINAL_TYPE_STRING_SET なので不要？
///         今のところ必須の用途が不明(20240605)
///         -> こちらを利用することになった(20240613)
/// 
///     VALUE_SET
///         その abstract class から派生する final class の value の set.
///     INSTANCE_SET
///         その abstract class から派生する final class の instance の set.
///     XXX
///         その abstract class から派生する 各 final class の instance を返す.
///         XXX には その abstract class の prefix または suffix を取り除いた host 部分を適用する.
///         ex:
///             Keyword を継承した ForKeyword という名前の final class があったとする.
///             Keyword.FOR_KEYWORD だと keyword の部分の意味が重複してしまう.
///             なので keyword という suffix を取り除き
///             Keyword.FOR と指定できるようにする.
///
/// {@endtemplate}
mixin EnumPattern
    implements
        PropertiesSignature
        ,ToJsonSignature
{

    /// {@template EnumPattern.value}
    /// 
    /// {@endtemplate}
    String get value;

    @override
    String toString() => value;

    @override
    Map<String, Object> get properties {
        return {
            'value': value,
        };
    }

    @override
    String toJson() {

        return value;

    }

    // /// {@template EnumPattern.hashCode}
    // /// 値が一切変化しないので 値が変わればそれに伴い変化する hascode も常に固定でよい（たぶん）.
    // /// {@endtemplate}
    // @override
    // int get hashCode {
    //     return 0;
    // }

    // /// runtimeType の等価だけみればよい.
    // /// operator == method - Object class - dart:core library - Dart API: "https://api.dart.dev/stable/2.15.1/dart-core/Object/operator_equals.html"
    // @override
    // bool operator ==(Object other) {
    //     if( other.runtimeType != runtimeType ) return false;
    //     return true;
    // }

}
