// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template ToJsonSignature}
/// 
/// ここでの toJson とは json へ encode 可能な形式へ変換するという意味で使われる.<br>
/// つまり json string を返すというわけではない.<br>
/// また serialize としても利用するため deserialize 可能でなければならない.<br>
/// 
/// {@endtemplate}
abstract class ToJsonSignature
{

    /// {@macro ToJsonSignature}
    Object toJson();

}
