// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template EnumPatternException}
/// {@endtemplate}
/// * [EnumPatternExceptionA]
/// {@macro EnumPatternExceptionA}
/// * [EnumPatternExceptionB]
/// {@macro EnumPatternExceptionB}
/// * [EnumPatternExceptionC]
/// {@macro EnumPatternExceptionC}
/// * [EnumPatternExceptionD]
/// {@macro EnumPatternExceptionD}
/// * [EnumPatternExceptionE]
/// {@macro EnumPatternExceptionE}
/// * [EnumPatternExceptionF]
/// {@macro EnumPatternExceptionF}
/// * [EnumPatternExceptionG]
/// {@macro EnumPatternExceptionG}
sealed class EnumPatternException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const EnumPatternException();

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template EnumPatternExceptionA}
/// old BPRiverOriginExceptionD.<br>
/// input invalid value to fromValue() constructor.<br>
/// {@endtemplate}
final class EnumPatternExceptionA
    extends
        EnumPatternException
{

    static const LOGGER_RESULT_MESSAGE = [
        'input invalid value to fromValue() constructor.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro EnumPatternExceptionA}
    const EnumPatternExceptionA();

}
