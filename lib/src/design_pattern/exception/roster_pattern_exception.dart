// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template RosterPatternException}
/// {@endtemplate}
/// * [RosterPatternExceptionA]
/// {@macro RosterPatternExceptionA}
/// * [RosterPatternExceptionB]
/// {@macro RosterPatternExceptionB}
/// * [RosterPatternExceptionC]
/// {@macro RosterPatternExceptionC}
/// * [RosterPatternExceptionD]
/// {@macro RosterPatternExceptionD}
/// * [RosterPatternExceptionE]
/// {@macro RosterPatternExceptionE}
/// * [RosterPatternExceptionF]
/// {@macro RosterPatternExceptionF}
/// * [RosterPatternExceptionG]
/// {@macro RosterPatternExceptionG}
sealed class RosterPatternException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const RosterPatternException();

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template RosterPatternExceptionA}
/// old BPRiverOriginExceptionA.<br>
/// exists duplicate primary key.
/// {@endtemplate}
class RosterPatternExceptionA
    extends
        RosterPatternException
{

    static const LOGGER_RESULT_MESSAGE =  [
        '重複した primary key があります.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro RosterPatternExceptionA}
    RosterPatternExceptionA();

}

/// {@template RosterPatternExceptionB}
/// old BPRiverOriginExceptionB.<br>
/// not exists targeted primary key at [RosterPattern.getByPrimaryKey].
/// {@endtemplate}
class RosterPatternExceptionB
    extends
        RosterPatternException
{

    static const LOGGER_RESULT_MESSAGE =  [
        'not exists targeted primary key at [RosterPattern.getByPrimaryKey].',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro RosterPatternExceptionB}
    RosterPatternExceptionB();

}
