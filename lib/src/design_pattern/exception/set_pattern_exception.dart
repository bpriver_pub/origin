// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template SetPatternException}
/// {@endtemplate}
/// * [SetPatternExceptionA]
/// {@macro SetPatternExceptionA}
/// * [SetPatternExceptionB]
/// {@macro SetPatternExceptionB}
/// * [SetPatternExceptionC]
/// {@macro SetPatternExceptionC}
/// * [SetPatternExceptionD]
/// {@macro SetPatternExceptionD}
/// * [SetPatternExceptionE]
/// {@macro SetPatternExceptionE}
/// * [SetPatternExceptionF]
/// {@macro SetPatternExceptionF}
/// * [SetPatternExceptionG]
/// {@macro SetPatternExceptionG}
sealed class SetPatternException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const SetPatternException();

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template SetPatternExceptionA}
/// old BPRiverOriginExceptionC.<br>
/// exists duplicate.
/// {@endtemplate}
class SetPatternExceptionA
    extends
        SetPatternException
{

    static const LOGGER_RESULT_MESSAGE = [
        'exists duplicate.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro SetPatternExceptionA}
    SetPatternExceptionA();

}
