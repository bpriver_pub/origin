// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template IterablePatternException}
/// {@endtemplate}
/// * [IterablePatternExceptionA]
/// {@macro IterablePatternExceptionA}
/// * [IterablePatternExceptionB]
/// {@macro IterablePatternExceptionB}
/// * [IterablePatternExceptionC]
/// {@macro IterablePatternExceptionC}
/// * [IterablePatternExceptionD]
/// {@macro IterablePatternExceptionD}
/// * [IterablePatternExceptionE]
/// {@macro IterablePatternExceptionE}
/// * [IterablePatternExceptionF]
/// {@macro IterablePatternExceptionF}
/// * [IterablePatternExceptionG]
/// {@macro IterablePatternExceptionG}
sealed class IterablePatternException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const IterablePatternException();

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template IterablePatternExceptionA}
/// old BPRiverOriginExceptionE.<br>
/// index is negative number at [IterablePattern.getByIndex].
/// {@endtemplate}
final class IterablePatternExceptionA
    extends
        IterablePatternException
    implements
        IterablePatternExceptionABC
{

    static const LOGGER_RESULT_MESSAGE = [
        'index is negative number at [IterablePattern.getByIndex].'
        // 'index が 負数.',
        // 'index = ${index}.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro IterablePatternExceptionA}
    IterablePatternExceptionA();

}

/// {@template IterablePatternExceptionB}
/// old BPRiverOriginExceptionF.<br>
/// index over length at [IterablePattern.getByIndex].
/// {@endtemplate}
final class IterablePatternExceptionB
    extends
        IterablePatternException
    implements
        IterablePatternExceptionABC
{

    static const LOGGER_RESULT_MESSAGE = [
        'index over length at [IterablePattern.getByIndex].'
        // 'index が length を超過.',
        // 'index = ${index}',
        // 'length = ${length}',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro IterablePatternExceptionB}
    IterablePatternExceptionB();

}

/// {@template IterablePatternExceptionC}
/// old BPRiverOriginExceptionG.<br>
/// length is zero(not exists element) at [IterablePattern.getByIndex].
/// {@endtemplate}
final class IterablePatternExceptionC
    extends
        IterablePatternException
    implements
        IterablePatternExceptionABC
{

    static const LOGGER_RESULT_MESSAGE = [
        'length is zero(not exists element) at [IterablePattern.getByIndex].',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro IterablePatternExceptionC}
    IterablePatternExceptionC();

}

/// old BPRiverOriginExceptionEFG.<br>
/// * [IterablePatternExceptionA]
/// {@macro IterablePatternExceptionA}
/// * [IterablePatternExceptionB]
/// {@macro IterablePatternExceptionB}
/// * [IterablePatternExceptionC]
/// {@macro IterablePatternExceptionC}
sealed class IterablePatternExceptionABC
    extends
        IterablePatternException
{}
