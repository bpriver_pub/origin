// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template InactivateSignature}
/// 
/// test 時 property の一部を無効化する(一定の値に変更する), 等価比較を行うための method.<br>
/// 例えば process id など, system によって割り振られる値をこちらで指定できないので 等価比較ができない.<br>
/// 
/// {@endtemplate}
abstract class InactivateSignature
{

    /// {@macro InactivateSignature}
    Object inactivate();

}
