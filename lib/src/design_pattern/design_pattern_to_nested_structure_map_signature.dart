// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

mixin DesignPatternToNestedStructureMapSignature
{

    /// {@template DesignPatternToNestedStructureMapSignature.toNestedStructureMap}
    /// 条件分岐で利用するため用意した.
    /// {@endtemplate}
    Map<String, Object> toNestedStructureMap();

}
