// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/design_pattern.dart';

/// {@template ValuePattern}
/// 
/// VT ・・・ ValueType
/// 
/// runtime constant(実行時定数, 修飾子は final) な値を 1つ([value]) だけ持つ class.
/// 
/// １．String toString() を適切な 文字列が返されるように override する。value.toString() で適切な文字列が返されるなら override の必要なし。
/// 
/// {@endtemplate}
mixin ValuePattern<
        VT extends Object
    >
    implements
        PropertiesSignature
        ,ToJsonSignature
{

    VT get value;

    @override
    int get hashCode {
        var result = 17;
        result = 37 * result + toString().hashCode;
        return result;
    }

    /// runtimeType と toString() の値を比較する。
    /// runtimeType が異なれば false.
    ///     super class であっても fasle となる.
    /// operator == method - Object class - dart:core library - Dart API: "https://api.dart.dev/stable/2.15.1/dart-core/Object/operator_equals.html"
    @override
    bool operator ==(Object other) {
        if( other.runtimeType != runtimeType ) return false;
        final typeChecked = other as ValuePattern;
        return typeChecked.toString() == toString();
    }

    @override
    String toString() => value.toString();

    @override
    Map<String, Object> get properties {
        
        if (value is EnumPattern) return {
            'value': value.toString()
        };

        return {
            'value': value,
        };

    }

    // ValuePattern は primitive type を VT に指定することを前提にしている.<br>
    // なので もしも VT に PropertiesSignature を実装しているものを指定した場合(enum pattern の場合) toJson() を呼び出す.
    @override
    Map<String, Object> toJson() {

        const lessThan = '<';

        final base = value;
        final type = runtimeType.toString();
        
        late final String head;
        late final Object body;

        if (base is ToJsonSignature) {
            body = base.toJson();
        } else {
            body = base;
        }
        
        final index = type.indexOf(lessThan);

        if (index == -1) {
            head = type;
        } else {
            head = type.substring(0, index);
        }

        return {
            head: {
                'value': body
            }
        };

    }

}
