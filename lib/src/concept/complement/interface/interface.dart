// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Interface}
/// [Signature] が 1つの slot や function に対する 型の構成を指すのに対し, <br>
/// [Interface] は 1つの class に対する型の構成(その class がどのような property や method を持っているかなど)を指す [Complement].
/// {@endtemplate}
abstract class Interface
    extends
        Complement
{}