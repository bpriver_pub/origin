// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

// null を許容する Mutable とは、null を許容するかどうかの違いしかないので、test 内容は全く同じで ok. なはず。
//      = operator の内容を、「NonNullableMutable<M> typeChecked = other;」にしなければならない
// null を許容しない型じゃないと、返り値に null を許容しない method を使う関係で、list などから、getbyId などしたいときに必要になる。
/// 
/// VT ・・・ ValueType
/// 
/// 値を動的に保持するが、null を許可しない。
/// 
class NonNullableObservable<
        VT extends Object
    >
    extends
        Complement
{

    /// 
    /// 複数の subscribe が可能。
    /// 
    NonNullableObservable.broadcast(this.value)
    :
        _controller = StreamController<VT>.broadcast()
    ;

    /// 
    /// 単一の subscribe のみ可能。
    /// 
    NonNullableObservable(this.value)
    :
        _controller = StreamController<VT>()
    ;

    final StreamController<VT> _controller;

    VT value;

    /// subscribe する。
    /// stream() が実行されるたびに、subscribe に渡した function が実行される。
    StreamSubscription<VT> subscribe(void Function(VT event) function) {
        return _controller.stream.listen((event) {
            function(event);
        });
    }

    /// 引数に渡した値を value に格納する。
    void next(VT value) {
        this.value = value;
        stream();
    }

    // null を stream する case.
    //      list を扱う場合、null を取り扱う必要が出てくる。
    //      なので、stream で null を許容している。
    /// value に格納された値を stream する。
    void stream() {
        _controller.sink.add(value);
    }

    @override
    String toString() => value.toString();

    @override
    int get hashCode {
        var result = 17;
        result = 37 * result + toString().hashCode;
        return result;
    }

    /// toString() の値を比較する。
    @override
    bool operator ==(Object other) {
        if( other.runtimeType != runtimeType ) return false;
        final typeChecked = other as NonNullableObservable<VT>;
        return typeChecked.toString() == toString();
    }

}