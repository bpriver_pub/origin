// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Specific}
/// [Signature] とは対照的に routine の 処理(実装)部分を指す [Complement].
/// {@endtemplate}
abstract class Specific
    extends
        Complement
{}