// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Signature}
/// routine の中でも slot や function に対する return type, name, argument などの 型情報の事を指す [Complement].
/// {@endtemplate}
abstract class Signature
    extends
        Complement
{}