// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Computation}
/// 
/// Concept を計量可能にするための Concept.
/// 
/// {@endtemplate}
abstract class Computation
    extends
        Complement
{
    
    Computation() {
        checkComputation();
    }

    const Computation.const$();

    Iterable<Object> get values;

    /// {@template Computation.checkComputation}
    /// called on constructor.
    /// {@endtemplate}
    void checkComputation();

}