// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Directive}
/// UI を構成するための [UserInterface].
/// {@endtemplate}
abstract class Directive
    extends
        UserInterface
{

    // c = current
    // l = location 
    // m = material
    // rt = runtimetype
    // d = debug
    // m = message
    // v = value
    // void printd(Object? object, [Object? value]) {
    //     /// この実装だと、第2引数に、null の何かしらの object が渡されたとき、xxx = null と表示されない
    //     /// 別の関数に分けるべきか？
    //     if(value != null) {
    //         print('$runtimeType: '.brightBlue.bold + '${object.toString()} = ' + value.toString());
    //     } else {
    //         print('$runtimeType: '.brightBlue.bold + object.    toString());
    //     }
    // }

    // void printm(Object object) {
    //     print('$runtimeType: '.brightBlue.bold + object.toString());
    // }
    
    // // １つにまとめると(printd)、第２引数に null が渡されたとき、「null」と表示されないので、実装。
    // void printv(String name, Object? value) {
    //     // print('$runtimeType: '.brightBlue.bold + '$name'.gray + ' = '.gray + value.toString());
    //     print('$runtimeType: '.brightBlue.bold + '$name'.brightBlue + ' = '.brightBlue + value.toString());
    // }
    
}