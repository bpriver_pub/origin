// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Terminal}
/// cui を通して user と I/O を行う.
/// {@endtemplate}
abstract class Terminal
    extends
        UserInterface
{
    const Terminal();
}