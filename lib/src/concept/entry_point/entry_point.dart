// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

// EntryPoint というのは 名前というより役割を指しているので Core という名前の方がよいか?(20231212)
/// {@template EntryPoint}
/// 概念のモデリングを行い定義する際に中核となる material.
/// 文字通り entry point の役割も担う.
/// {@endtemplate}
abstract class EntryPoint
    extends
        Concept
{

    const EntryPoint();

}