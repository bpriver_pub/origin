// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Enforcer}
/// [UserInterface] から 受け取った data を基に [Model] と [Relay] を操作し その結果を [UserInterface] へと返す [EntryPoint].
/// {@endtemplate}
abstract class Enforcer
    extends
        EntryPoint
{

    const Enforcer();

}
