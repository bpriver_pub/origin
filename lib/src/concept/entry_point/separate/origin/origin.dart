// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Origin}
/// プログラムを開発する際の根底となるような機能で 相互依存させた方が都合の良いもの.
/// {@endtemplate}
abstract class Origin
    extends
        Separate
{}