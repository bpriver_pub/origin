// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Module}
/// [Application] を構成する一番大まかな単位の [Separate].
/// 複数の  routine を集約する routine.
/// [Application] は module の集まりで構成される.
/// {@endtemplate}
abstract class Module
    extends
        Separate
{

    const Module();
    
    // Announcement startAnnounce(String functionName, String message) {
    //     return Announcement(functionName, runtimeType, message);
    // }

    // /// method の開始を print する.
    // void startAnnounce(String functionName) {
    //     print('Announcement: '.brightBlue.bold + '$runtimeType.$functionName()' + ' start');
    // }
    
    // /// method の終了を print する
    // T exitAnnounce<T>(String functionName, T returnValue) {
    //     print('Announcement: '.brightBlue.bold + '$runtimeType.$functionName()' + ' exit');
    //     return returnValue;
    // }

}

/// {@template Announcement}
/// ## [Announcement]
/// instance の 生成と同時に start を print する.
/// {@endtemplate}
class Announcement
{

    // final String functionName;
    // final Type type;

    // /// {@macro Announcement}
    // Announcement(this.functionName, this.type, String message) {
    //     _start(message);
    // }

    // /// method の開始を print する.
    // void _start(String message) {
    //     print('Announcement:'.brightBlue.bold + ' ' + '${message}' + ' ' + '$type.$functionName()');
    // }

    // /// method の終了を print する
    // T exit<T>(T returnValue) {
    //     print('Announcement:'.brightBlue.bold + ' ' + 'exit' + ' ' + '$type.$functionName()');
    //     return returnValue;
    // }

}