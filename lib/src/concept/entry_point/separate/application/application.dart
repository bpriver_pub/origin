// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Application}
/// 不特定多数の end user から利用されることを目的とした routine を集約する routine.
/// developer からではなく end user の利用を想定して作られる.
/// {@endtemplate}
abstract class Application
    extends
        Separate
{}