// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/concept.dart';

/// {@template Unified}
/// unified model
/// 
/// 環境や読み込むシステムによって、同じ概念を指しているのだが、primitive な値が変化してしまうもの。変化させないと、同じ概念を指し示せない情報。
/// 例えば、key 情報。key code や virtual key code など、正しく識別するために適用されている identifier が、環境ごとに異なる。
/// この違いを吸収した方がプログラム内で、扱いやすくなり、単純な記述ミスが減る。
/// 
/// {@endtemplate}
abstract class Unified
    extends
        Model
{

    const Unified();

}