// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/bpriver_yaml.dart';

/// {@template BpriverYamlException}
/// {@endtemplate}
sealed class BpriverYamlException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    // YamlWriter.write がどの exception を投げるか調べるのめんどくさいので Exception にしている.
    static BpriverYamlExceptionI fromException(Exception exception) => BpriverYamlExceptionI();

    const BpriverYamlException();

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template BpriverYamlExceptionA}
/// 1つの task file に複数の yaml document が含まれています.
/// {@endtemplate}
final class BpriverYamlExceptionA
    extends
        BpriverYamlException
{

    static const LOGGER_RESULT_MESSAGE = [
        '1つの task file に複数の yaml document が含まれています.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionA}
    const BpriverYamlExceptionA();

}

/// {@template BpriverYamlExceptionB}
/// yaml document contents is not yaml list.
/// {@endtemplate}
final class BpriverYamlExceptionB
    extends
        BpriverYamlException
{

    static const LOGGER_RESULT_MESSAGE = [
        'yaml document contents is not yaml list.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionB}
    const BpriverYamlExceptionB();

}

/// {@template BpriverYamlExceptionC}
/// yaml document contents is not yaml map.
/// {@endtemplate}
final class BpriverYamlExceptionC
    extends
        BpriverYamlException
{

    static const LOGGER_RESULT_MESSAGE = [
        'yaml document contents is not yaml map.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionC}
    const BpriverYamlExceptionC();

}

/// {@template BpriverYamlExceptionD}
/// 指定された yaml map の中には 指定した key は存在しませんでした.
/// {@endtemplate}
final class BpriverYamlExceptionD
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDE
        ,BpriverYamlExceptionDEF
        ,BpriverYamlExceptionDEFH
        ,BpriverYamlExceptionDFH
        ,BpriverYamlExceptionDH
        ,BpriverYamlExceptionDEH
{

    static const LOGGER_RESULT_MESSAGE = [
        '指定された yaml map の中には 指定した key は存在しませんでした.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionD}
    const BpriverYamlExceptionD();

}

/// {@template BpriverYamlExceptionE}
/// 指定された yaml map の中で 指定された key の値の type が 指定したものと異なりました.
/// {@endtemplate}
final class BpriverYamlExceptionE
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDE
        ,BpriverYamlExceptionDEF
        ,BpriverYamlExceptionDEFH
        ,BpriverYamlExceptionEF
        ,BpriverYamlExceptionDEH
{

    static const LOGGER_RESULT_MESSAGE = [
        '指定された yaml map の中で 指定された key の値の type が 指定したものと異なりました.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionE}
    const BpriverYamlExceptionE();

}

/// {@template BpriverYamlExceptionF}
/// 指定された element は 指定された type ではありませんでした.
/// {@endtemplate}
final class BpriverYamlExceptionF
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDEF
        ,BpriverYamlExceptionEF
        ,BpriverYamlExceptionDEFH
        ,BpriverYamlExceptionDFH
{

    static const LOGGER_RESULT_MESSAGE = [
        '指定された element は 指定された type ではありませんでした.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionF}
    const BpriverYamlExceptionF();

}

/// {@template BpriverYamlExceptionG}
/// 指定された yaml map の長さが 指定された長さと異なる.
/// {@endtemplate}
final class BpriverYamlExceptionG
    extends
        BpriverYamlException
{

    static const LOGGER_RESULT_MESSAGE = [
        '指定された yaml map の長さが 指定された長さと異なる.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionG}
    const BpriverYamlExceptionG();

}

/// {@template BpriverYamlExceptionH}
/// 指定の yaml map から 指定した key で想定外の type の value を取得した.
/// {@endtemplate}
final class BpriverYamlExceptionH
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDH
        ,BpriverYamlExceptionDEFH
        ,BpriverYamlExceptionDFH
        ,BpriverYamlExceptionDEH
{

    static const LOGGER_RESULT_MESSAGE = [
        '指定の yaml map から 指定した key で想定外の type の value を取得した.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionH}
    const BpriverYamlExceptionH();

}

/// {@template BpriverYamlExceptionI}
/// failed to convert to yaml.<br>
/// {@endtemplate}
final class BpriverYamlExceptionI
    extends
        BpriverYamlException
{

    static const LOGGER_RESULT_MESSAGE = [
        'failed to convert to yaml.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionI}
    const BpriverYamlExceptionI();

}

/// {@template BpriverYamlExceptionJ}
/// yaml document contents type is not T.<br>
/// {@endtemplate}
final class BpriverYamlExceptionJ
    extends
        BpriverYamlException
{

    static const LOGGER_RESULT_MESSAGE = [
        'yaml document contents type is not T.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro BpriverYamlExceptionJ}
    const BpriverYamlExceptionJ();

}

/// * [BpriverYamlExceptionD]
/// {@macro BpriverYamlExceptionD}
/// * [BpriverYamlExceptionE]
/// {@macro BpriverYamlExceptionE}
sealed class BpriverYamlExceptionDE
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDEF
        ,BpriverYamlExceptionDEFH
        ,BpriverYamlExceptionDEH
{}

/// * [BpriverYamlExceptionD]
/// {@macro BpriverYamlExceptionD}
/// * [BpriverYamlExceptionE]
/// {@macro BpriverYamlExceptionE}
/// * [BpriverYamlExceptionF]
/// {@macro BpriverYamlExceptionF}
sealed class BpriverYamlExceptionDEF
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDEFH
{}

/// * [BpriverYamlExceptionD]
/// {@macro BpriverYamlExceptionD}
/// * [BpriverYamlExceptionF]
/// {@macro BpriverYamlExceptionF}
/// * [BpriverYamlExceptionH]
/// {@macro BpriverYamlExceptionH}
sealed class BpriverYamlExceptionDFH
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDEFH
{}

/// * [BpriverYamlExceptionE]
/// {@macro BpriverYamlExceptionE}
/// * [BpriverYamlExceptionF]
/// {@macro BpriverYamlExceptionF}
sealed class BpriverYamlExceptionEF
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDEF
        ,BpriverYamlExceptionDEFH
{}

/// * [BpriverYamlExceptionD]
/// {@macro BpriverYamlExceptionD}
/// * [BpriverYamlExceptionH]
/// {@macro BpriverYamlExceptionH}
sealed class BpriverYamlExceptionDH
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDEFH
        ,BpriverYamlExceptionDFH
        ,BpriverYamlExceptionDEH
{}

/// * [BpriverYamlExceptionD]
/// {@macro BpriverYamlExceptionD}
/// * [BpriverYamlExceptionE]
/// {@macro BpriverYamlExceptionE}
/// * [BpriverYamlExceptionH]
/// {@macro BpriverYamlExceptionH}
sealed class BpriverYamlExceptionDEH
    extends
        BpriverYamlException
    implements
        BpriverYamlExceptionDEFH
{}

/// * [BpriverYamlExceptionD]
/// {@macro BpriverYamlExceptionD}
/// * [BpriverYamlExceptionE]
/// {@macro BpriverYamlExceptionE}
/// * [BpriverYamlExceptionF]
/// {@macro BpriverYamlExceptionF}
/// * [BpriverYamlExceptionH]
/// {@macro BpriverYamlExceptionH}
sealed class BpriverYamlExceptionDEFH
    extends
        BpriverYamlException
{}
