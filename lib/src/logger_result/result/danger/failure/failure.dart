// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

final class Failure<
        V extends Object
        ,E extends Exception
    >
    extends
        Danger<V, E>
{

    @override
    final E wrapped;
    @override
    final Log log;

    Failure(this.wrapped, [Log? log])
    :
        log = log ?? Log.empty()
    ;

    /// {@template Failure.throwOut}
    /// 
    /// RT...return type<br>
    /// ET...exception type<br>
    /// BET...base exception type<br>
    /// 
    /// exception を throw out させる syntax sugar.<br>
    /// 
    /// {@endtemplate}
    static Failure<RT, ET> throwOut<
        RT extends Object
        ,ET extends Exception
        ,BET extends Exception
    >(
        BET baseException,
        Error error,
        [
            Log? log,
            Map<String, dynamic> monitorOnError = const {},
        ]
    ) {

        switch (baseException) {
            case ET():
                return Failure(baseException, log);
            case _:
                if (log != null) log.monitor(monitorOnError);
                Result.panic(error, log);
        }
        
    }

    @override
    Never get asExpected => Result.panic(LoggerResultlErrorA(), Log(classLocation: Failure<V, E>, functionLocation: 'asExpected'));

    @override
    E get asException => wrapped;
    
}
