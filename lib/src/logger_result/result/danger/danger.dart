// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template Danger}
/// [Result] との違いは [E] が Object ではなく Exception であること.<br>
/// 成功もしくは失敗の可能性があることを示す型.<br>
/// V で void を指定したい場合, 代わりに [Complete] を指定する.<br>
/// {@endtemplate}
sealed class Danger<
        V extends Object
        ,E extends Exception
    >
    extends
        Result<V>
{

    const Danger();

    static Danger<V, E> fromDanger<V extends Object, E extends Exception>(
        Danger<V,E> danger,
        [
            Log? log,
        ]
    ) {
        switch (danger) {
            case Success(): return Success(danger.wrapped, log);
            case Failure(): return Failure(danger.wrapped, log);
        }
    }

    /// {@template Danger.throwOut}
    /// 
    /// RT...return type<br>
    /// ET...exception type<br>
    /// BET...base exception type<br>
    /// 
    /// exception を throw out させる syntax sugar.<br>
    /// 
    /// [monitorOnError] は [log] が null の時は追加されない.
    /// 
    /// {@endtemplate}
    static Danger<RT, ET> throwOut<
        RT extends Object
        ,ET extends Exception
        ,BET extends Exception
    >(
        Danger<RT,BET> danger,
        Error error,
        [
            Log? log,
            Map<String, dynamic> monitorOnError = const {},
        ]
    ) {

        switch (danger) {
            case Success():
                return Success(danger.wrapped, log);
            case Failure(asException: final exception):
                switch (exception) {
                    case ET():
                        return Failure(exception, log);
                    case _:
                        if (log != null) log.monitor(monitorOnError);
                        Result.panic(error, log);
                }
        }
        
    }

    /// {@template Danger.tryAndConvert}
    /// 
    /// VT...value type<br>
    /// CET...catch exception type<br>
    /// RET...return exception type<br>
    /// 
    /// exception or failure を throw する可能性のある function を実行し, もし exception が throw された 場合 その exception を catch し, 新しい exception に convert する sugar constructor.<br>
    /// 
    /// catch した [Failure] の log を渡すために [log] は必須.
    /// 
    /// {@endtemplate}
    static Danger<VT, RET> tryAndConvert<
        VT extends Object
        ,CET extends Exception
        ,RET extends Exception
    >(
        VT Function() tryAction,
        RET Function(CET e) returnExceptionFactory,
        Log log,
    ) {

        try {

            final tryActionResult = tryAction();
            return Success(tryActionResult, log);

        // exception と failure の両方に対応する.
        // とりあえず.
        } on CET catch (e) {

            final exception = returnExceptionFactory(e);
            // convert 前の exception を log に記録する.
            return Failure(exception, log.monitor({'exception': e}));

        } on Failure<Object, CET> catch (e) {

            // log 情報をしっかり受け取る.
            log.add(e);
            final exception = returnExceptionFactory(e.wrapped);
            return Failure(exception, log);

        }
        
    }

    /// {@template Danger.tryAndConvertFuture}
    /// 
    /// VT...value type<br>
    /// CET...catch exception type<br>
    /// RET...return exception type<br>
    /// 
    /// exception or failure を throw する可能性のある asynchronous function を実行し, もし exception が throw された 場合 その exception を catch し, 新しい exception に convert する sugar constructor.<br>
    /// 
    /// catch した [Failure] の log を渡すために [log] は必須.
    /// 
    /// {@endtemplate}
    static FutureDanger<VT, RET> tryAndConvertFuture<
        VT extends Object
        ,CET extends Exception
        ,RET extends Exception
    >(
        Future<VT> Function() tryAction,
        RET Function(CET e) returnExceptionFactory,
        Log log,
    ) async {

        try {

            final tryActionResult = await tryAction();
            return Success(tryActionResult, log);

        } on CET catch (e) {

            final exception = returnExceptionFactory(e);
            // convert 前の exception を log に記録する.
            return Failure(exception, log.monitor({'exception': e}));

        } on Failure<Object, CET> catch (e) {

            // log 情報をしっかり受け取る.
            log.add(e);
            final exception = returnExceptionFactory(e.wrapped);
            return Failure(exception, log);

        }

    }

    /// {@template Danger.tryAndCatch}
    /// 
    /// RT...return type<br>
    /// ET...exception type<br>
    /// 
    /// [tryAction] を実行し,
    /// もし exception or [Failure] が throw された 場合
    /// それ を catch し
    /// 返す syntax sugar.<br>
    /// 
    /// catch した [Failure] の log を渡すために [log] は必須.
    /// 
    /// {@endtemplate}
    static Danger<RT, ET> tryAndCatch<
        RT extends Object
        ,ET extends Exception
    >(
        RT Function() tryAction,
        Log log,
    ) {

        // final log = Log(classLocation: Danger, functionLocation: 'tryAndCatch');

        try {

            final value = tryAction();
            return Success(value, log);

        } on ET catch (e) {

            return Failure(e, log);

        } on Failure<Object, ET> catch (e) {

            log.add(e);
            return Failure(e.wrapped, log);

        }

    }

    /// {@template Danger.tryAndCatchFuture}
    /// 
    /// RT...return type<br>
    /// ET...exception type<br>
    /// 
    /// [tryAction] を実行し,
    /// もし exception or [Failure] が throw された 場合
    /// それ を catch し
    /// 返す syntax sugar.<br>
    /// 
    /// catch した [Failure] の log を渡すために [log] は必須.
    /// 
    /// {@endtemplate}
    static FutureDanger<RT, ET> tryAndCatchFuture<
        RT extends Object
        ,ET extends Exception
    >(
        Future<RT> Function() tryAction,
        Log log,
    ) async {

        try {

            final value = await tryAction();
            return Success(value, log);

        } on ET catch (e) {

            return Failure(e, log);

        } on Failure<Object, ET> catch (e) {

            log.add(e);
            return Failure(e.wrapped, log);

        }

    }

    @override
    E get asException;

    @override
    Never get asError => Result.panic(LoggerResultlErrorC(), Log(classLocation: Danger<V, E>, functionLocation: 'asError'));

}
