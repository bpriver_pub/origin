// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

final class Success<
        V extends Object
        ,E extends Exception
    >
    extends
        Danger<V, E>
{

    @override
    final V wrapped;
    @override
    final Log log;

    /// {@macro LogPart._const}
    const Success.const$(this.wrapped, [this.log = const Log.const$(NotExist, '', {})]);

    Success(this.wrapped, [Log? log])
    :
        log = log ?? Log.empty()
    ;

    /// {@template Success.throwOut}
    /// 
    /// RT...return type<br>
    /// ET...exception type<br>
    /// BET...base exception type<br>
    /// 
    /// exception を throw out させる syntax sugar.<br>
    /// return type が Success で固定の場合などの時に利用することがある.<br>
    /// 
    /// {@endtemplate}
    static Success<RT, ET> throwOut<
        RT extends Object
        ,ET extends Exception
    >(
        Danger<RT, Exception> danger,
        Error error,
        [
            Log? log,
            Map<String, dynamic> monitorOnError = const {},
        ]
    ) {

        switch (danger) {
            case Success():
                return Success(danger.wrapped, log);
            case Failure():
                if (log != null) {
                    log.monitor(monitorOnError);
                    log.monitor({'exception': danger.wrapped});
                };
                Result.panic(error, log);
        }
        
    }

    @override
    V get asExpected => wrapped;

    @override
    Never get asException => Result.panic(LoggerResultlErrorB(), Log(classLocation: Success<V, E>, functionLocation: 'asException'));

}
