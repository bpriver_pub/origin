// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

// Panic class は public じゃないと test の時に 型指定できなくなってしまう.
/// {@template Panic}
/// Error が発生したことを表す型.<br>
/// プログラムを停止させる処理に利用する.<br>
/// [Error] をそのまま throw しただけでは log の情報が付与されないため [Panic] に [Error] を wrap する必要がある.
/// {@endtemplate}
final class Panic<
        E extends Error
    >
    extends
        Result<E>
{

    @override
    final E wrapped;
    @override
    final Log log;

    /// {@macro Panic}
    Panic(this.wrapped, [Log? log])
    :
        log = log ?? Log.empty()
    ;

    /// {@template Panic.asError}
    /// ## Caution
    /// [asError] is unsafe code.<br>
    /// Throwable [LoggerResultlErrorB] wrapped in [Panic] then [wrapped] is not [E].<br>
    /// This method's main uses is simple notation for test code.<br>
    /// {@endtemplate}
    E get asError => wrapped;

    @override
    Never get asExpected => Result.panic(LoggerResultlErrorA(), Log(classLocation: Panic<E>, functionLocation: 'asExpected'));

    @override
    Never get asException => Result.panic(LoggerResultlErrorB(), Log(classLocation: Panic<E>, functionLocation: 'asException'));

}
