// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

final class Survive<
        V extends Object
        ,EX extends Exception
        ,ER extends Error
    >
    extends
        Disaster<V, EX, ER>
{

    @override
    final V wrapped;
    @override
    final Log log;

    /// {@macro LogPart._const}
    const Survive.const$(this.wrapped, [this.log = const Log.const$(NotExist, '', {})]);

    Survive(this.wrapped, [Log? log])
    :
        log = log ?? Log.empty()
    ;

    @override
    V get asExpected => wrapped;

    @override
    Never get asException => Result.panic(LoggerResultlErrorB(), Log(classLocation: Survive<V, EX, ER>, functionLocation: 'asException'));

}
