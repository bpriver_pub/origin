// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template Disaster}
/// [Risk] と [Danger] の両方を伴うことを表す型.
/// V で void を指定したい場合, 代わりに [Complete] を指定する.<br>
/// {@endtemplate}
sealed class Disaster<
        V extends Object
        ,EX extends Exception
        ,ER extends Error
    >
    extends
        Result<V>
{

    const Disaster();

    // /// {@macro LogPart._const}
    // const Disaster.const$(this.wrapped, [this.log = const Log.const$(NotExist, '', {})]);

    // @override
    // V get asExpected => switch(wrapped) {
    //     V() => wrapped as V,
    //     _ => Result.panic(LoggerResultlErrorA(), Log(classLocation: Disaster<V, EX, ER>, functionLocation: 'asExpected')),
    // };

    // @override
    // EX get asException => switch(wrapped) {
    //     EX() => wrapped as EX,
    //     _ => Result.panic(LoggerResultlErrorB(), Log(classLocation: Disaster<V, EX, ER>, functionLocation: 'asException')),
    // };

    @override
    V get asExpected;

    @override
    EX get asException;

    @override
    Never get asError => Result.panic(LoggerResultlErrorC(), Log(classLocation: Disaster<V, EX, ER>, functionLocation: 'asError'));

}
