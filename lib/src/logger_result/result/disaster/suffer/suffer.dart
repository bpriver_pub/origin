// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

final class Suffer<
        V extends Object
        ,EX extends Exception
        ,ER extends Error
    >
    extends
        Disaster<V, EX, ER>
{

    @override
    final EX wrapped;
    @override
    final Log log;

    /// {@macro LogPart._const}
    const Suffer.const$(this.wrapped, [this.log = const Log.const$(NotExist, '', {})]);

    Suffer(this.wrapped, [Log? log])
    :
        log = log ?? Log.empty()
    ;

    @override
    Never get asExpected => Result.panic(LoggerResultlErrorA(), Log(classLocation: Suffer<V, EX, ER>, functionLocation: 'asExpected'));

    @override
    EX get asException => wrapped;

}
