// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template Risk}
/// 呼び出し側の handling で回避しなければならない error が存在することを示す型.<br>
/// なので 単純に error を throw する記述があることを示す型ではない.<br>
/// 例えば exception handling で throw out で error を throw する記述をするが その error は この Risk 型で示す対象ではない.<br>
/// [V] で void を指定したい場合, 代わりに [Complete] を指定する.<br>
/// error だった場合 throw されるので 返り値は は success のみである.<br>
/// また このときの [E] は error があることを呼び出し側に示すためだけにあるので 表記できればそれで仕事はおわりである.<br>
/// {@endtemplate}
final class Risk<
        V extends Object
        ,E extends Error
    >
    extends
        Result<V>
{

    @override
    final V wrapped;
    @override
    final Log log;

    Risk(this.wrapped, [Log? log])
    :
        log = log ?? Log.empty()
    ;

    /// {@macro LogPart._const}
    const Risk.const$(this.wrapped, [this.log = const Log.const$(NotExist, '', {})]);

    @override
    V get asExpected => wrapped;

    @override
    Never get asException => Result.panic(LoggerResultlErrorB(), Log(classLocation: Risk<V, E>, functionLocation: 'asException'));

    @override
    Never get asError => Result.panic(LoggerResultlErrorC(), Log(classLocation: Risk<V, E>, functionLocation: 'asError'));

}
