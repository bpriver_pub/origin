// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

// 各 final class で 受け取った Log に対して _setResultTypeAndResultMessage() を実行しなければならないためここで行う.
/// {@template Result}
/// 
/// {@endtemplate}
sealed class Result<
        V extends Object
        // ,EX extends Exception
        // ,ER extends Error
    >
    with
        AggregationPattern
    implements
        LoggerResultInterface
{

    @override
    Object get wrapped;
    @override
    Log get log;

    const Result();

    /// {@macro Panic}
    static Never panic<E extends Error>(E wrapped, [Log? log]) {

        final result = Panic<E>(wrapped, log);

        throw result;

    }

    /// {@template Result.tryAndRethrow}
    /// 
    /// RT...return type<br>
    /// CT...catch error type<br>
    /// ET...custom error type<br>
    /// 
    /// error を throw する可能性のある function を実行し, もし error が throw された 場合 その error を catch し, 新しい custom error を rethrow する.<br>
    /// 手動で try catch を書いた時に起こりえる error の rethrow し忘れ防止のための syntax sugar.<br>
    /// 
    /// {@endtemplate}
    static RT tryAndRethrow<RT, CT extends Error, ET extends Error>(RT Function() function, ET Function(CT e) customErrorFactory){
        try {
            return function();
        } on CT catch (e) {
            throw customErrorFactory(e);
        }
    }

    @override
    Map<String, Object> get properties {
        return {
            'wrapped': wrapped,
            'log': log,
        };
    }

    History toHistory() {
        return History.fromResult(this);
    }

    // test の時に全部の result にあった方が都合が良い.
    /// {@template Result.asExpected}
    /// ## Caution
    /// [asExpected] is unsafe code.<br>
    /// Throwable [LoggerResultlErrorA] wrapped in [Panic] then [wrapped] is not [V].<br>
    /// This method's main uses is simple notation for test code.<br>
    /// {@endtemplate}
    V get asExpected;

    /// {@template Result.asException}
    /// ## Caution
    /// [asException] is unsafe code.<br>
    /// Throwable [LoggerResultlErrorB] wrapped in [Panic] then [wrapped] is not [Exception].<br>
    /// This method's main uses is simple notation for test code.<br>
    /// {@endtemplate}
    Exception get asException;

    /// {@template Result.asError}
    /// ## Caution
    /// [asError] is unsafe code.<br>
    /// Throwable [LoggerResultlErrorB] wrapped in [Panic] then [wrapped] is not [Error].<br>
    /// This method's main uses is simple notation for test code.<br>
    /// {@endtemplate}
    Error get asError;

}
