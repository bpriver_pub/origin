// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template Safety}
/// [Danger] とは対照的に 必ず 成功した結果を返すことを示す型.<br>
/// V で void を指定したい場合, 代わりに [Complete] を指定する.<br>
/// {@endtemplate}
final class Safety<
        V extends Object
    >
    extends
        Result<V>
{

    @override
    final V wrapped;
    @override
    final Log log;

    Safety(this.wrapped, [Log? log])
    :
        log = log ?? Log.empty()
    ;

    /// {@macro LogPart._const}
    const Safety.const$(this.wrapped, [this.log = const Log.const$(NotExist, '', {})]);

    /// {@template Safety.throwOut}
    /// 
    /// RT...return type<br>
    /// CT...catch exception type<br>
    /// 
    /// exception を throw out させる syntax sugar.<br>
    /// 
    /// [monitorOnError] は [log] が null の時は追加されない.
    /// 
    /// {@endtemplate}
    static Safety<RT> throwOut<
        RT extends Object
        ,CT extends Exception
    >(
        Danger<RT,CT> result,
        Error error,
        [
            Log? log,
            Map<String, dynamic> monitorOnError = const {},
        ]
    ) {

        switch (result) {
            case Success(asExpected: final value):
                return Safety(value, log);
            case Failure():
                if (log != null) log.monitor(monitorOnError);
                Result.panic(error, log);
        }
        
    }

    @override
    V get asExpected => wrapped;

    @override
    Never get asException => Result.panic(LoggerResultlErrorB(), Log(classLocation: Safety<V>, functionLocation: 'asException'));

    @override
    Never get asError => Result.panic(LoggerResultlErrorC(), Log(classLocation: Safety<V>, functionLocation: 'asError'));

}
