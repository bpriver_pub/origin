// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

typedef FutureDanger<V extends Object, E extends Exception> = Future<Danger<V, E>>;
typedef FutureSuccess<V extends Object, E extends Exception> = Future<Success<V, E>>;
typedef FutureFailure<V extends Object, E extends Exception> = Future<Failure<V, E>>;
typedef FutureSafety<V extends Object> = Future<Safety<V>>;
typedef FutureResult<V extends Object, EX extends Exception, ER extends Error> = Future<Result<V>>;
typedef FutureComplete = Future<Complete>;
typedef FutureDisaster<V extends Object, EX extends Exception, ER extends Error> = Future<Disaster<V, EX, ER>>;

// これは可読性が下がる？
//      横に長くなりすぎるためスクロールしないと確認できないためめんどくさい.
//      定義するのは手間だが 呼び出し側で 下記のように定義する
//      typedef ExceptionAB = ExceptionUnino2<ExceptionA, ExceptionB>;

// typedef Danger2<V extends Object, E1 extends Exception, E2 extends Exception> = Danger<V, ExceptionUnion2<E1, E2>>;
// typedef Danger3<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception> = Danger<V, ExceptionUnion3<E1, E2, E3>>;
// typedef Danger4<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception> = Danger<V, ExceptionUnion4<E1, E2, E3, E4>>;
// typedef Danger5<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception, E5 extends Exception> = Danger<V, ExceptionUnion5<E1, E2, E3, E4, E5>>;

// typedef FutureDanger2<V extends Object, E1 extends Exception, E2 extends Exception> = Future<Danger<V, ExceptionUnion2<E1, E2>>>;
// typedef FutureDanger3<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception> = Future<Danger<V, ExceptionUnion3<E1, E2, E3>>>;
// typedef FutureDanger4<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception> = Future<Danger<V, ExceptionUnion4<E1, E2, E3, E4>>>;
// typedef FutureDanger5<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception, E5 extends Exception> = Future<Danger<V, ExceptionUnion5<E1, E2, E3, E4, E5>>>;

// typedef Success2<V extends Object, E1 extends Exception, E2 extends Exception> = Success<V, ExceptionUnion2<E1, E2>>;
// typedef Success3<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception> = Success<V, ExceptionUnion3<E1, E2, E3>>;
// typedef Success4<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception> = Success<V, ExceptionUnion4<E1, E2, E3, E4>>;
// typedef Success5<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception, E5 extends Exception> = Success<V, ExceptionUnion5<E1, E2, E3, E4, E5>>;

// typedef FutureSuccess2<V extends Object, E1 extends Exception, E2 extends Exception> = Future<Success<V, ExceptionUnion2<E1, E2>>>;
// typedef FutureSuccess3<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception> = Future<Success<V, ExceptionUnion3<E1, E2, E3>>>;
// typedef FutureSuccess4<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception> = Future<Success<V, ExceptionUnion4<E1, E2, E3, E4>>>;
// typedef FutureSuccess5<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception, E5 extends Exception> = Future<Success<V, ExceptionUnion5<E1, E2, E3, E4, E5>>>;

// typedef Failure2<V extends Object, E1 extends Exception, E2 extends Exception> = Failure<V, ExceptionUnion2<E1, E2>>;
// typedef Failure3<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception> = Failure<V, ExceptionUnion3<E1, E2, E3>>;
// typedef Failure4<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception> = Failure<V, ExceptionUnion4<E1, E2, E3, E4>>;
// typedef Failure5<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception, E5 extends Exception> = Failure<V, ExceptionUnion5<E1, E2, E3, E4, E5>>;

// typedef FutureFailure2<V extends Object, E1 extends Exception, E2 extends Exception> = Future<Failure<V, ExceptionUnion2<E1, E2>>>;
// typedef FutureFailure3<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception> = Future<Failure<V, ExceptionUnion3<E1, E2, E3>>>;
// typedef FutureFailure4<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception> = Future<Failure<V, ExceptionUnion4<E1, E2, E3, E4>>>;
// typedef FutureFailure5<V extends Object, E1 extends Exception, E2 extends Exception, E3 extends Exception, E4 extends Exception, E5 extends Exception> = Future<Failure<V, ExceptionUnion5<E1, E2, E3, E4, E5>>>;
