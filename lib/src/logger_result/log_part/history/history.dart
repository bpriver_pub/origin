// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

// yaml や json に変換する機能は持たせていない
//      もたせてしまうと bpriver_origin が依存する library が 増えてしまう.
//      持たせないことで呼び出し側は好きな library を選択できる.
//      yaml や json 関連の handling が不要なのでその分 保守が楽になる.
//      ここで行うのは mold() による 成形した Map を返すところまで.

/// {@template History}
/// {@endtemplate}
class History
    extends
        LogPart
{

    final Type resultType;

    /// {@template History.message}
    /// [Result.wrapped] が [LoggerResultMessageSignature] を実装していた場合 ここに受け取る.
    /// {@endtemplate}
    final List<String> message;

    History(this.resultType, this.message, super.classLocation, super.functionLocation, super._monitor, super._debug, super._historyList);

    factory History.fromResult(LoggerResultInterface loggerResult) {
        
        List<String> message = [];
        final wrapped = loggerResult.wrapped;

        if (wrapped is LoggerResultMessageSignature) message = wrapped.loggerResultMessage;

        return History(
            wrapped.runtimeType,
            message,
            loggerResult.log.classLocation,
            loggerResult.log.functionLocation,
            loggerResult.log.getMonitor(),
            loggerResult.log.getDebug(),
            loggerResult.log.getHistoryList(),
        );
    }

    @override
    Map<String, Object> get properties {
        return {
            'resultType': resultType,
            'message': message,
            'classLocation': classLocation,
            'functionLocation': functionLocation,
            '_monitor': _monitor,
            '_debug': _debug,
            '_historyList': _historyList,
        };
    }

    History change({
        Type? resultType,
        List<String>? message,
        Type? classLocation,
        String? functionLocation,
        Map<String, dynamic>? monitor,
        Map<String, dynamic>? debug,
        List<History>? historyList,
    }) {
        return History(
            resultType ?? this.resultType,
            message ?? this.message,
            classLocation ?? this.classLocation,
            functionLocation ?? this.functionLocation,
            monitor ?? this._monitor,
            debug ?? this._debug,
            historyList ?? this._historyList,
        );
    }

    List<MapEntry<String, dynamic>> _moldB() {
        return List.generate(
            _monitor.length,
            (index) => MapEntry<String, dynamic>(
                _monitor.keys.elementAt(index) + '(monitor)',
                _monitor.values.elementAt(index),
            )
        );
    }

    List<MapEntry<String, dynamic>> _moldC() {
        return List.generate(
            _debug.length,
            (index) => MapEntry<String, dynamic>(
                _debug.keys.elementAt(index) + '(debug)',
                _debug.values.elementAt(index),
            )
        );
    }

    List<MapEntry<String, dynamic>> _moldD() {
        return List.generate(
            _historyList.length,
            (index) => MapEntry(
                _historyList.elementAt(index).location + '(${index + 1})',
                _historyList.elementAt(index)._moldA(),
            )
        );
    }

    Map<String, dynamic> _moldA() {
        final typeEntry = MapEntry('result', resultType.toString());
        final messageEntry = MapEntry('message', message);
        final monitorMapEntries = _moldB();
        final debugMapEntries = _moldC();
        final historyListMapEntries = _moldD();
        final entries = [
            typeEntry,
            if (message.isNotEmpty) messageEntry,
            ...monitorMapEntries,
            ...debugMapEntries,
            ...historyListMapEntries,
        ];
        return Map.fromEntries(entries);
    }

    Map<String, dynamic> mold() {
        return {
            location: _moldA(),
        };
    }

}
