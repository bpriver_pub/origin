// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template Log}
/// {@endtemplate}
class Log
    extends
        LogPart
{

    /// {@macro LogPart._const}
    const Log.const$([Type classLocation = NotExist, String functionLocation = '', Map<String, dynamic> monitor = const {}])
    :
        super._const(classLocation, functionLocation, monitor)
    ;

    // const にすると 内部的に unmodifiabl map and list になるのでつけてはいけない
    Log.empty()
    :
        super(
            NotExist,
            '',
            {},
            {},
            [],
        )
    ;

    /// {@template Log}
    /// {@endtemplate}
    Log({
        Type classLocation = NotExist,
        String functionLocation = '',
        Map<String, dynamic>? monitor,
        Map<String, dynamic>? debug,
        List<History>? historyList,
    })
    :
        super(
            classLocation,
            functionLocation,
            monitor ?? {},
            debug ?? {},
            historyList ?? [],
        )
    ;

    // if の return 文などで 1行で書きたいときに log を返す必要がある.
    // ex:
    //      return Safety(sample, log.monitor('a', 'x'));

    Log monitor(Map<String, dynamic> map) {
        _monitor.addAll(map);
        return this;
    }

    Log debug(Map<String, dynamic> map) {

        if (bool.fromEnvironment(LogPart._dart_vm_product)) return Log.empty();

        _debug.addAll(map);

        return this;

    }

    /// {@template Log.add}
    /// [_historyList] に追加する.
    /// {@endtemplate}
    void add(LoggerResultInterface loggerLoggerResultInterface) {
        final history = History.fromResult(loggerLoggerResultInterface);
        _historyList.add(history);
    }

    @override
    Map<String, Object> get properties {
        return {
            'classLocation': classLocation,
            'functionLocation': functionLocation,
            '_monitor': _monitor,
            '_debug': _debug,
            '_historyList': _historyList,
        };
    }

}
