// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

// short hand version の 各種 toYaml は key 名の万が一の被りを防ぐために 適宜 suffix を付けている.
// 形式としては ( + <何かしらの説明や番号> + ) である.
//     ex. (monitor), (debug), (1), など.
// key 名が被った場合 後から設定した key により上書きされてしまう.
abstract class LogPart
    with
        AggregationPattern
{

    static const _dart_vm_product = 'dart.vm.product';

    /// {@template LogPart.classLocation}
    /// {@endtemplate}
    final Type classLocation;

    /// {@template LogPart.functionLocation}
    /// {@endtemplate}
    final String functionLocation;

    // 外から内容を削除されないために private.
    /// {@template LogPart.monitor}
    /// 本番時および開発時に記録する data.
    /// {@endtemplate}
    final Map<String, dynamic> _monitor;

    // 外から内容を削除されないために private.
    /// {@template LogPart.debug}
    /// 開発時のみに記録する data.
    /// {@endtemplate}
    final Map<String, dynamic> _debug;

    // 外から内容を削除されないために private.
    /// {@template LogPart.historyList}
    /// {@endtemplate}
    final List<History> _historyList;

    /// {@template LogPart._const}
    /// 実行速度重視のための実装の const constructor.<br>
    /// そのため [Log._monitor], [Log._debug], [Log._historyList] は const で定義され 追加の操作ができず 最低限の情報([Log.classLocation], [Log.functionLocation])しか利用できない.<br>
    /// {@endtemplate}
    const LogPart._const(this.classLocation, this.functionLocation, this._monitor)
    :
        // debug は 本番環境では追加されない仕様の都合上 const では設定ができない.
        _debug = const {}
        ,_historyList = const []
    ;

    LogPart(this.classLocation, this.functionLocation, this._monitor, Map<String, dynamic> debug, this._historyList)
    :
        // debug は debug 実行時のみ表示する. 本番実行時は表示しない.
        _debug = bool.fromEnvironment(_dart_vm_product) ? const {} : debug
    ;

    String get location {

        final functionPart = functionLocation + '()';

        if (classLocation == NotExist) return functionPart;

        final compound = classLocation.toString() + '.' + functionPart;

        return compound;
        
    }

    Map<String, dynamic> getMonitor() => Map.from(_monitor);
    
    Map<String, dynamic> getDebug() => Map.from(_debug);

    List<History> getHistoryList() => List.from(_historyList);

    // 主に test 用に用意した method である.
    // constructor で Failure を throw し catch したときの monitor などの値の確認にとても手間がかかかってしまう
    // result constructor の時点では複数の history に入れ子になっている状態.
    // また throw をされた Failure を catch し 値を確認するのは面倒
    // なので result constructor の test に統合できるようにしたい
    /// {@template LogPart.getHistory}
    /// class の指定が存在しない場合 [classLocation] で [NotExist] を指定する.<br>
    /// {@endtemplate}
    List<History> getHistory(Type classLocation, String functionLocation) {

        final List<History> list = [];

        for (final history in _historyList) {

            if (
                history.classLocation == classLocation &&
                history.functionLocation == functionLocation
            ) {
                list.add(history);
            }
            
        }

        for (final history in _historyList) {

            final getHistoryResult = history.getHistory(classLocation, functionLocation);
            list.addAll(getHistoryResult);
            
        }

        return list;

    }

}