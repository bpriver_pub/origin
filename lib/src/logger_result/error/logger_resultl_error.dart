// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template LoggerResultlError}
/// {@endtemplate}
/// * [LoggerResultlErrorA]
/// {@macro LoggerResultlErrorA}
/// * [LoggerResultlErrorB]
/// {@macro LoggerResultlErrorB}
/// * [LoggerResultlErrorC]
/// {@macro LoggerResultlErrorC}
/// * [LoggerResultlErrorD]
/// {@macro LoggerResultlErrorD}
/// * [LoggerResultlErrorE]
/// {@macro LoggerResultlErrorE}
/// * [LoggerResultlErrorF]
/// {@macro LoggerResultlErrorF}
/// * [LoggerResultlErrorG]
/// {@macro LoggerResultlErrorG}
sealed class LoggerResultlError
    extends
        Error
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}

/// {@template LoggerResultlErrorA}
/// [Result.wrapped] is not expected.
/// Can't call [Result.asExpected].
/// {@endtemplate}
final class LoggerResultlErrorA
    extends
        LoggerResultlError
{

    static const LOGGER_RESULT_MESSAGE = [
        'Result.wrapped is not expected.',
        'Can\'t call Result.asExpected.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro LoggerResultlErrorA}
    LoggerResultlErrorA();

}

/// {@template LoggerResultlErrorB}
/// [Result.wrapped] is not exception.
/// Can't call [Result.asException].
/// {@endtemplate}
final class LoggerResultlErrorB
    extends
        LoggerResultlError
{

    static const LOGGER_RESULT_MESSAGE = [
        'Result.wrapped is not exception.',
        'Result.wrapped is value.',
        'Can\'t call Result.asException.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro LoggerResultlErrorB}
    LoggerResultlErrorB();

}

/// {@template LoggerResultlErrorC}
/// [Result.wrapped] is not error.
/// Can't call [Result.asError].
/// {@endtemplate}
final class LoggerResultlErrorC
    extends
        LoggerResultlError
{

    static const LOGGER_RESULT_MESSAGE = [
        'Result.wrapped is not error.',
        'Can\'t call Result.asError.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro LoggerResultlErrorC}
    LoggerResultlErrorC();

}

/// {@template LoggerResultlErrorD}
/// never reached proccess.
/// {@endtemplate}
final class LoggerResultlErrorD
    extends
        LoggerResultlError
{

    static const LOGGER_RESULT_MESSAGE = [
        'never reached proccess.',
    ];

    @override
    List<String> get loggerResultMessage => LOGGER_RESULT_MESSAGE;

    /// {@macro LoggerResultlErrorD}
    LoggerResultlErrorD();

}
