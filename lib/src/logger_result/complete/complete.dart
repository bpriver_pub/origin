// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template Complete}
/// [Result] もしくは [Safety] の V に void を指定する代わりに利用するための型.<br>
/// V が dynamic または Object? を許容できるようにすると 静的型解析が働かず 型解析に関する実行時 error の危険が生じてしまい その場合どこで error が発生したのかわからず追跡が難しい.<br>
/// {@endtemplate}
final class Complete
    with
        AggregationPattern
    implements
        LoggerResultMessageSignature
{

    final List<String> loggerResultMessage;

    const Complete([this.loggerResultMessage = const []]);
    
    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}
