// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template NotExist}
/// [Log.classLocation] と [Log.resultType] を指定しないに場合に適用するためだけに存在する class.
/// {@endtemplate}
abstract final class NotExist {}
