// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

/// {@template LoggerResultException}
/// {@endtemplate}
sealed class LoggerResultException
    with
        AggregationPattern
    implements
        Exception
        ,LoggerResultMessageSignature
{

    const LoggerResultException();

    @override
    List<String> get loggerResultMessage;

    @override
    Map<String, Object> get properties {
        return {
            'loggerResultMessage': loggerResultMessage
        };
    }

}
