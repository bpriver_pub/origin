// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

// 主に native logger result を log として追加できるようにするため.
/// {@template LoggerResultInterface}
/// [LoggerResultInterface] を implements した class を log として追加できるように.
/// {@endtemplate}
abstract class LoggerResultInterface
{

    Object get wrapped;
    Log get log;

}
