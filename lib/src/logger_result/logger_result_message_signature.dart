// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

part of 'package:bpriver_origin/logger_result.dart';

// message signature を class にひとつずつ implements しないといけないが
//     incident(exception,error) に関して
// 　　　    package 内で inheritence tree を構成してもらう前提.
//         なので incident tree の root 1つに implements すればそれで ok.
//     普通の class に関して
// 　　　　　　success 時の class には ひとつずつ implements しなければいけないが
//　　　　　　　必ず message を付けないといけない、ということではなく ケースバイケースのはずなので
// 　　　　　　１つ１つ implemnets させても、code を記述する負担は高くならない。

/// {@template LoggerResultMessageSignature}
/// この class を implements した class が [Log.wrapped] に設定されたとき [LoggerResultMessageSignature.loggerResultMessage] の中身が shortHand 表記でも log で表示されるようになる.
/// {@endtemplate}
abstract class LoggerResultMessageSignature
{

    List<String> get loggerResultMessage;

}