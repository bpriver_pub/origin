// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library design_pattern;

import 'dart:collection';

import 'package:bpriver_origin/concept.dart';
import 'package:bpriver_origin/logger_result.dart';

part 'src/design_pattern/error/aggregation_pattern_error.dart';
part 'src/design_pattern/error/iterable_pattern_iterator_error.dart';
part 'src/design_pattern/error/enum_pattern_error.dart';

part 'src/design_pattern/exception/iterable_pattern_exception.dart';
part 'src/design_pattern/exception/roster_pattern_exception.dart';
part 'src/design_pattern/exception/set_pattern_exception.dart';
part 'src/design_pattern/exception/enum_pattern_exception.dart';

part 'src/design_pattern/properties_signature.dart';
part 'src/design_pattern/to_json_signature.dart';
part 'src/design_pattern/inactivate_signature.dart';
part 'src/design_pattern/design_pattern_to_nested_structure_map_entry_signature.dart';
part 'src/design_pattern/design_pattern_to_nested_structure_map_signature.dart';

part 'src/design_pattern/aggregation/aggregation_pattern_to_shorthand_signature.dart';
part 'src/design_pattern/aggregation/aggregation_pattern.dart';
part 'src/design_pattern/entity/entity_pattern.dart';
part 'src/design_pattern/iterable/iterable_pattern.dart';
part 'src/design_pattern/iterable/iterable_pattern_iterator.dart';
part 'src/design_pattern/iterable/iterable_pattern_internal_factory.dart';
part 'src/design_pattern/iterable/list/list_pattern.dart';
part 'src/design_pattern/iterable/list/list_pattern_private_member_tester.dart';
part 'src/design_pattern/iterable/set/set_pattern.dart';
part 'src/design_pattern/iterable/roster/roster_pattern.dart';
part 'src/design_pattern/iterable/roster/roster_pattern_put.dart';
part 'src/design_pattern/iterable/roster/roster_pattern_put_all.dart';

part 'src/design_pattern/value/value_pattern.dart';

part 'src/design_pattern/enum/enum_pattern.dart';