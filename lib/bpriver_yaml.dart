// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library bpriver_yaml;

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:yaml/yaml.dart';
import 'package:yaml_writer/yaml_writer.dart';

part 'src/bpriver_yaml/error/bpriver_yaml_error.dart';
part 'src/bpriver_yaml/exception/bpriver_yaml_exception.dart';

abstract class BpriverYaml
{

    const BpriverYaml();

    // yaml_writer/lib/src/yaml_writer.dart at master · gmpassos/yaml_writer · GitHub: "https://github.com/gmpassos/yaml_writer/blob/master/lib/src/yaml_writer.dart"
    static Object? _toEncodable(dynamic dynamic) {

        if (dynamic is Type) return dynamic.toString();

        if (dynamic is Iterable) return dynamic.toList();

        try {
            return dynamic.toJson();
        } catch (e) {
            return dynamic.toString();
        }

    }

    /// {@template BpriverYaml.toYamlString}
    /// output yaml string.<br>
    /// {@endtemplate}
    static Danger<String, BpriverYamlExceptionI> toYamlString(Map<String, dynamic> map, [int indentSize = 4]) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'toYamlString');

        final yamlWriter = YamlWriter(
            indentSize: indentSize,
            // 文字列を出力する際 可能であれば、クォートで囲まない.
            allowUnquotedStrings: true,
            toEncodable: _toEncodable,
        );

        return Danger.tryAndConvert(
            () => yamlWriter.write(map),
            BpriverYamlException.fromException,
            log,
        );

    }

    static Danger<YamlDocument, BpriverYamlExceptionA> parseYamlStringAsSingleYamlDocument(String yamlString) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'parseYamlStringAsSingleYamlDocument');

        late final YamlDocument loadYamlDocumentResult;

        // loadYaml function - yaml library - Dart API: "https://pub.dev/documentation/yaml/latest/yaml/loadYaml.html"
        // document から excepiton or error が throw されるため Danger.tryAncCatch が使えない.
        try {
            
            loadYamlDocumentResult = loadYamlDocument(yamlString);

        } on YamlException catch (e) {

            return Failure(BpriverYamlExceptionA(), log.monitor({'yaml exception': e}));
          
        } on  ArgumentError catch (e) {

            return Failure(BpriverYamlExceptionA(), log.monitor({'argument error': e}));

        }

        return Success(loadYamlDocumentResult, log);

    }

    /// {@template parseYamlDocumentAsT}
    /// [YamlMap], [YamlList], [YamlScalar].
    /// {@endtemplate}
    static Danger<T, BpriverYamlExceptionJ> parseYamlDocumentAsT<T extends YamlNode>(YamlDocument yamlDocument) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'parseYamlDocumentAsT');

        final contents = yamlDocument.contents;
        if (contents is! T) return Failure(BpriverYamlExceptionJ(), log.monitor({
            'T': T,
            'contents': contents,
            'type': contents.runtimeType,
        }));

        return Success(contents, log);

    }

    /// {@template BpriverYaml.getByKeyFromYamlMapAsT}
    /// available type as [T]
    /// * [String]
    /// * [int]
    /// * [bool]
    /// * [YamlMap]
    /// * [YamlList]
    /// {@endtemplate}
    static Danger<T, BpriverYamlExceptionDE> getByKeyFromYamlMapAsT<T extends Object>(YamlMap yamlMap, String key) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'getByKeyFromYamlMapAsT');

        // 1
        final containsKeyResult = yamlMap.containsKey(key);
        if (!containsKeyResult) return Failure(BpriverYamlExceptionD(), log.monitor({
            'yaml map': yamlMap,
            'key': key,
        }));

        // 2. 1 で検証済みなので return.
        final value = yamlMap[key];
        if (value is! T) return Failure(BpriverYamlExceptionE(), log.monitor({
            'yaml map': yamlMap,
            'key': key,
            'value': value,
            'value type': value.runtimeType,
            'T': T
        }));
        
        return Success(value, log);

    }

    static Danger<T, BpriverYamlExceptionDH> getByKeyFromYamlMapThenAction<T extends Object>(
        YamlMap yamlMap,
        String key,
        {
            T Function(YamlMap value)? thenMap,
            T Function(YamlList value)? thenList,
            T Function(String value)? thenString,
            T Function(bool value)? thenBoolean,
            T Function(num value)? thenNumber,
            T Function()? thenNull,
        }
    ) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'getByKeyFromYamlMapThenAction');

        // 1
        final containsKeyResult = yamlMap.containsKey(key);
        if (!containsKeyResult) return Failure(BpriverYamlExceptionD(), log.monitor({
            'yaml map': yamlMap,
            'key': key,
        }));

        // 2. 1 で検証済みなので return.
        final value = yamlMap[key];

        switch (value) {
        case YamlMap():
            if (thenMap == null) continue exceptionCase;
            return Success(thenMap(value), log);
        case YamlList():
            if (thenList == null) continue exceptionCase;
            return Success(thenList(value), log);
        case String():
            if (thenString == null) continue exceptionCase;
            return Success(thenString(value), log);
        case bool():
            if (thenBoolean == null) continue exceptionCase;
            return Success(thenBoolean(value), log);
        case num():
            if (thenNumber == null) continue exceptionCase;
            return Success(thenNumber(value), log);
        case null:
            if (thenNull == null) continue exceptionCase;
            return Success(thenNull(), log);
        exceptionCase:
        case _:
            final validTypes = <String>[
                if (thenMap != null) 'YamlMap',
                if (thenList != null) 'YamlList',
                if (thenString != null) 'String',
                if (thenBoolean != null) 'bool',
                if (thenNumber != null) 'number',
                if (thenNull != null) 'null',
            ];
            return  Failure(BpriverYamlExceptionH(), log.monitor({
                'value': value,
                'value type': value.runtimeType,
                'valid types': validTypes,
            }));
        }

    }

    static Danger<T, BpriverYamlExceptionF> parseDynamicAsT<T extends Object>(dynamic element) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'parseDynamicAsT');

        if (element is! T) return Failure(BpriverYamlExceptionF(), log.monitor({
            'element': element,
            'element type': element.runtimeType,
            'required type': T,
        }));

        return Success(element, log);

    }

    // parseAsDynamic では cast ができなかったため実装.
    static Danger<List<T>, BpriverYamlExceptionF> parseYamlListAsT<T extends Object>(YamlList yamlList) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'parseYamlListAsT');

        final List<T> list = [];

        for (final element in yamlList) {

            if (element is! T) return Failure(BpriverYamlExceptionF(), log.monitor({
                'element': element,
                'element type': element.runtimeType,
                'required type': T,
            }));

            list.add(element);
            
        }

        return Success(list, log);

    }

    static Danger<Complete, BpriverYamlExceptionG> validateYamlMapLength(YamlMap yamlMap, int length) {

        final log = Log(classLocation: BpriverYaml, functionLocation: 'validateYamlMapLength');

        if (yamlMap.length != 1) return Failure(BpriverYamlExceptionG(), log.monitor({
            'yaml map': yamlMap,
            'yaml map length': yamlMap.length,
            'required length': length,
        }));

        return Success(Complete(), log);

    }

}
