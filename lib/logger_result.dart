// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library logger_result;

import 'package:bpriver_origin/design_pattern.dart';

part 'src/logger_result/error/logger_resultl_error.dart';
part 'src/logger_result/exception/logger_result_exception.dart';

part 'src/logger_result/logger_result_typedef.dart';
part 'src/logger_result/logger_result_message_signature.dart';
part 'src/logger_result/logger_result_interface.dart';

part 'src/logger_result/result/result.dart';
part 'src/logger_result/result/panic/panic.dart';
part 'src/logger_result/result/risk/risk.dart';
part 'src/logger_result/result/safety/safety.dart';
part 'src/logger_result/result/disaster/disaster.dart';
part 'src/logger_result/result/disaster/survive/survive.dart';
part 'src/logger_result/result/disaster/suffer/suffer.dart';
part 'src/logger_result/result/danger/danger.dart';
part 'src/logger_result/result/danger/success/success.dart';
part 'src/logger_result/result/danger/failure/failure.dart';

part 'src/logger_result/log_part/log_part.dart';
part 'src/logger_result/log_part/history/history.dart';
part 'src/logger_result/log_part/log/log.dart';

part 'src/logger_result/not_exist/not_exist.dart';

part 'src/logger_result/complete/complete.dart';
