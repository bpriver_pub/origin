// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

library concept;

import 'dart:async';

part 'src/concept/complement/complement.dart';

part 'src/concept/complement/computation/computation.dart';

part 'src/concept/complement/interface/interface.dart';

part 'src/concept/complement/observable/non_nullable/non_nullable_observable.dart';
part 'src/concept/complement/observable/nullable/nullable_observable.dart';

part 'src/concept/complement/viewpoint/viewpoint.dart';

part 'src/concept/complement/signature/signature.dart';
part 'src/concept/complement/specific/specific.dart';

part 'src/concept/entry_point/entry_point.dart';
part 'src/concept/entry_point/relay.dart';

part 'src/concept/entry_point/enforcer/enforcer.dart';

part 'src/concept/entry_point/model/model.dart';
part 'src/concept/entry_point/model/setting/setting.dart';
part 'src/concept/entry_point/model/signal/signal.dart';
part 'src/concept/entry_point/model/staging/staging.dart';
part 'src/concept/entry_point/model/subject/subject.dart';
part 'src/concept/entry_point/model/thin/thin.dart';
part 'src/concept/entry_point/model/unified/unified.dart';

part 'src/concept/entry_point/separate/separate.dart';
part 'src/concept/entry_point/separate/barrel/barrel.dart';
part 'src/concept/entry_point/separate/layer/layer.dart';
part 'src/concept/entry_point/separate/library/library.dart';
part 'src/concept/entry_point/separate/module/module.dart';
part 'src/concept/entry_point/separate/monolith/monolith.dart';
part 'src/concept/entry_point/separate/application/application.dart';
part 'src/concept/entry_point/separate/origin/origin.dart';

part 'src/concept/entry_point/user_interface/user_interface.dart';
part 'src/concept/entry_point/user_interface/directive/directive.dart';
part 'src/concept/entry_point/user_interface/terminal/terminal.dart';

sealed class Concept
{

    const Concept();

}
