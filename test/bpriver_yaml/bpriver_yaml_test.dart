// Copyright (C) 2023, the bpriver_yaml project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

// ignore_for_file: unused_import

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';
import 'package:yaml/yaml.dart';

class MockA {}
class MockB {}
class MockC {}

sealed class ExceptionMock implements Exception {}
class ExceptionMockA extends ExceptionMock {}
class ExceptionMockB extends ExceptionMock {}
class ExceptionMockC extends ExceptionMock {}

void main() {

    group('toYamlString', () {
        final historyC = History(MockC, ['message c'], MockC, 'methodC', {'monitor key c': 'monitor value c'}, {'debug key c': 'debug value c'}, []);
        final historyB = History(MockB, ['message b'], MockB, 'methodB', {'monitor key b': 'monitor value b'}, {'debug key b': 'debug value b'}, []);
        final historyA = History(MockA, ['message a'], MockA, 'methodA', {'monitor key a': 'monitor value a'}, {'debug key a': 'debug value a'}, [
            historyB,
            historyC,
        ]);
        test('expected: return String then ', () {
            final map = historyA.mold();
            final indentSize = 4;
            final base = BpriverYaml.toYamlString(map, indentSize);
            final result = base;
            final actual = result.wrapped;
            final expected = 
'''MockA.methodA():
    result: MockA
    message:
        -   message a
    monitor key a(monitor): monitor value a
    debug key a(debug): debug value a
    MockB.methodB()(1):
        result: MockB
        message:
            -   message b
        monitor key b(monitor): monitor value b
        debug key b(debug): debug value b
    MockC.methodC()(2):
        result: MockC
        message:
            -   message c
        monitor key c(monitor): monitor value c
        debug key c(debug): debug value c
''';
            expect(actual, expected);
        });
    });

    group('parseYamlStringAsSingleYamlDocument', () {
        test('expected: return YamlDocument then ', () {
            final yamlString = '';
            final base = BpriverYaml.parseYamlStringAsSingleYamlDocument(yamlString);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlDocument>();
            expect(actual, expected);
        });
        test('exception: return BpriverYamlExceptionA then 複数の yaml document が含まれる.', () {
            final yamlString = '''
a: a
---
b: b
''';
            final base = BpriverYaml.parseYamlStringAsSingleYamlDocument(yamlString);
            final result = base;
            final actual = result.wrapped;
            final expected = BpriverYamlExceptionA();
            expect(actual, expected);
        });
    });

    group('parseYamlDocumentAsT<YamlList>', () {
        test('expected: return YamlList', () {
            final yamlString = '''
- a
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final base = BpriverYaml.parseYamlDocumentAsT<YamlList>(yamlDocument);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlList>();
            expect(actual, expected);
        });
        test('expected: return YamlMap', () {
            final yamlString = '''
a: a
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final base = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlMap>();
            expect(actual, expected);
        });
        test('expected: return YamlScalar then string', () {
            final yamlString = 'a';
            final yamlDocument = loadYamlDocument(yamlString);
            final base = BpriverYaml.parseYamlDocumentAsT<YamlScalar>(yamlDocument);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlScalar>();
            expect(actual, expected);
        });
        test('expected: return YamlScalar then number', () {
            final yamlString = '0';
            final yamlDocument = loadYamlDocument(yamlString);
            final base = BpriverYaml.parseYamlDocumentAsT<YamlScalar>(yamlDocument);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlScalar>();
            expect(actual, expected);
        });
        test('expected: return YamlScalar then bool', () {
            final yamlString = 'true';
            final yamlDocument = loadYamlDocument(yamlString);
            final base = BpriverYaml.parseYamlDocumentAsT<YamlScalar>(yamlDocument);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlScalar>();
            expect(actual, expected);
        });
        test('expected: return YamlScalar then null', () {
            final yamlString = 'null';
            final yamlDocument = loadYamlDocument(yamlString);
            final base = BpriverYaml.parseYamlDocumentAsT<YamlScalar>(yamlDocument);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlScalar>();
            expect(actual, expected);
        });
        test('exception: return BpriverYamlExceptionJ then contents type is not T.', () {
            final yamlString = '''
a: a
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final base = BpriverYaml.parseYamlDocumentAsT<YamlList>(yamlDocument);
            final result = base;
            final actual = result.wrapped;
            final expected = BpriverYamlExceptionJ();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'T': YamlList,
                    'contents': yamlDocument.contents,
                    'type': YamlMap,
                };
                expect(actual, expected);
            }
        });
    });

    group('getByKeyFromYamlMapAsT', () {
        test('expected: return String then ', () {
            final yamlString = '''
a: 'x'
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'a';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<String>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = 'x';
            expect(actual, expected);
        });
        test('expected: return int then ', () {
            final yamlString = '''
a: 1
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'a';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<int>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = 1;
            expect(actual, expected);
        });
        test('expected: return bool then ', () {
            final yamlString = '''
a: true
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'a';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<bool>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return YamlMap then ', () {
            final yamlString = '''
a:
    b:
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'a';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<YamlMap>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlMap>();
            expect(actual, expected);
        });
        test('expected: return YamlList then type a', () {
            final yamlString = '''
a: 
    - x
    - y
    - z
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'a';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<YamlList>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlList>();
            expect(actual, expected);
        });
        test('expected: return YamlList then type b', () {
            final yamlString = '''
a: [x, y, z]
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'a';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<YamlList>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlList>();
            expect(actual, expected);
        });
        test('exception: return BpriverYamlExceptionD then key not exists', () {
            final yamlString = '''
a:
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'dummy';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<YamlList>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = BpriverYamlExceptionD();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'yaml map': yamlMap,
                    'key': key,
                };
                expect(actual, expected);
            }
        });
        test('exception: return BpriverYamlExceptionE then value type is not T', () {
            final yamlString = '''
a: 'x'
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final key = 'a';
            final base = BpriverYaml.getByKeyFromYamlMapAsT<int>(yamlMap, key);
            final result = base;
            final actual = result.wrapped;
            final expected = BpriverYamlExceptionE();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'yaml map': yamlMap,
                    'key': key,
                    'value': 'x',
                    'value type': String,
                    'T': int,
                };
                expect(actual, expected);
            }
        });
    });

    group('parseDynamicAsT', () {
        test('expected: return YamlMap then element is T', () {
            final yamlString = '''
- a:
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlList = BpriverYaml.parseYamlDocumentAsT<YamlList>(yamlDocument).asExpected;
            final element = yamlList.first;
            final base = BpriverYaml.parseDynamicAsT<YamlMap>(element);
            final result = base;
            final actual = result.wrapped;
            final expected = isA<YamlMap>();
            expect(actual, expected);
        });
        test('exception: return BpriverYamlExceptionF then element is not T', () {
            final yamlString = '''
- 'a'
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlList = BpriverYaml.parseYamlDocumentAsT<YamlList>(yamlDocument).asExpected;
            final element = yamlList.first;
            final base = BpriverYaml.parseDynamicAsT<YamlMap>(element);
            final result = base;
            final actual = result.wrapped;
            final expected = BpriverYamlExceptionF();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'element': element,
                    'element type': String,
                    'required type': YamlMap,
                };
                expect(actual, expected);
            }
        });
    });

    group('parseYamlListAsT', () {
        test('expected: return List then elements is T', () {
            final yamlString = '''
- 'a'
- 'b'
- 'c'
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlList = BpriverYaml.parseYamlDocumentAsT<YamlList>(yamlDocument).asExpected;
            final base = BpriverYaml.parseYamlListAsT<String>(yamlList);
            final result = base;
            final actual = result.wrapped;
            final expected = ['a', 'b', 'c'];
            expect(actual, expected);
        });
        test('exception: return BpriverYamlExceptionF then elements is not T', () {
            final yamlString = '''
- 'a'
- 1
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlList = BpriverYaml.parseYamlDocumentAsT<YamlList>(yamlDocument).asExpected;
            final base = BpriverYaml.parseYamlListAsT<String>(yamlList);
            final result = base;
            final actual = result.wrapped;
            final expected = BpriverYamlExceptionF();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'element': 1,
                    'element type': int,
                    'required type': String,
                };
                expect(actual, expected);
            }
        });
    });

    group('validateYamlMapLength', () {
        test('expected: return Complete then length is match', () {
            final yamlString = '''
a: 'x'
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final length = 1;
            final base = BpriverYaml.validateYamlMapLength(yamlMap, length);
            final result = base;
            final actual = result.wrapped;
            final expected = Complete();
            expect(actual, expected);
        });
        test('exception: return BpriverYamlExceptionG then length not match', () {
            final yamlString = '''
a: 'x'
b: 'y'
''';
            final yamlDocument = loadYamlDocument(yamlString);
            final yamlMap = BpriverYaml.parseYamlDocumentAsT<YamlMap>(yamlDocument).asExpected;
            final length = 1;
            final base = BpriverYaml.validateYamlMapLength(yamlMap, length);
            final result = base;
            final actual = result.wrapped;
            final expected = BpriverYamlExceptionG();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'yaml map': yamlMap,
                    'yaml map length': 2,
                    'required length': length,
                };
                expect(actual, expected);
            }
        });
    });

}
