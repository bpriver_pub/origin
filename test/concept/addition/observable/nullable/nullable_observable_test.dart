// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/concept.dart';
import 'package:test/test.dart';

class NullableObservableValuePatternMock
    extends
        NullableObservable<Object>
{
    NullableObservableValuePatternMock(Object? value) : super(value);
}

class NullableObservableValuePatternMock2
    extends
        NullableObservable<Object>
{
    NullableObservableValuePatternMock2(Object? value) : super(value);
}

void main(){

    // hashCode と == は、model に toString() が実装されている必要があるので、NullableObservableValuePatternMockを使う。

    // group('get value', () {
    //     test('equals', () {
    //         expect(observableA.value, modelA);
    //     });
    // });

    group('next()', () {
        test('equals', () {
            final base = NullableObservableValuePatternMock('x');
            base.next('a');
            expect(base.value, 'a');
            base.next('b');
            expect(base.value, 'b');
        });
    });
    
    group('toString()', () {
        test('equals', () {
            final observableA = NullableObservableValuePatternMock('a');
            expect(observableA.toString(), observableA.value.toString());
        });
    });

    group('subscribe', () {
        // test('equals', () {
        //     expect(observableA.value, valueA);
        // });
    });

    group('stream', () {
        // test('equals', () {
        //     expect(observableA.value, valueA);
        // });
    });

    group('get hashCode', () {
        final observableA = NullableObservableValuePatternMock('a');
        final observableB = NullableObservableValuePatternMock('b');
        final otherA = NullableObservableValuePatternMock2('a');
        final otherA2 = NullableObservableValuePatternMock2('a');
        test('value が同じなら、hashCode の値も変わらない', () {
            expect(observableA.value, equals(otherA.value));
            expect(otherA.hashCode, equals(otherA2.hashCode));
        });
        test('not equals', () {
            expect(observableA.hashCode == observableB.hashCode, equals(false));
        });
    });

    group('operator ==', () {
        final observableA = NullableObservableValuePatternMock('a');
        final observableB = NullableObservableValuePatternMock('b');
        test('true', () {
            expect(observableA, observableA);
        });
        test('not equals value => false', () {
            expect(observableA == observableB, false);
        });
        test('not equals type => false', () {
            final otherA = NullableObservableValuePatternMock2('a');
            expect(observableA.toString(), otherA.toString());
            expect(observableA == otherA, equals(false));
        });
    });

}