// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/concept.dart';
import 'package:test/test.dart';

class NonNullableObservableValuePatternMock
    extends
        NonNullableObservable<Object>
{
    NonNullableObservableValuePatternMock(Object value) : super(value);
}

class NonNullableObservableValuePatternMock2
    extends
        NonNullableObservable<Object>
{
    NonNullableObservableValuePatternMock2(Object value) : super(value);
}

void main(){

    // hashCode と == は、model に toString() が実装されている必要があるので、ObservableValuePatternMockを使う。

    group('get value', () {
        test('equals', () {
            final base = NonNullableObservableValuePatternMock('a');
            final actual = base.value;
            final expected = 'a';
            expect(actual, expected);
        });
    });

    group('next()', () {
        test('equals', () {
            final base = NonNullableObservableValuePatternMock('x');
            final actual = base;
            final expectedA = 'a';
            final expectedB = 'b';
            actual.next('a');
            expect(actual.value, expectedA);
            actual.next('b');
            expect(actual.value, expectedB);
        });
    });
    
    group('toString()', () {
        test('equals', () {
            final observableA = NonNullableObservableValuePatternMock('a');
            expect(observableA.toString(), observableA.value.toString());
        });
    });

    group('subscribe', () {
        // test('equals', () {
        //     expect(observableA.value, valueA);
        // });
    });

    group('stream', () {
        // test('equals', () {
        //     expect(observableA.value, valueA);
        // });
    });

    group('get hashCode', () {
        final otherA = NonNullableObservableValuePatternMock2('a');
        final otherA2 = NonNullableObservableValuePatternMock2('a');
        test('value が同じなら、hashCode の値も変わらない', () {
            final observableA = NonNullableObservableValuePatternMock('a');
            expect(observableA.value, equals(otherA.value));
            expect(otherA.hashCode, equals(otherA2.hashCode));
        });
        test('not equals', () {
            final observableA = NonNullableObservableValuePatternMock('a');
            final observableB = NonNullableObservableValuePatternMock('b');
            expect(observableA.hashCode == observableB.hashCode, equals(false));
        });
    });

    group('operator ==', () {
        test('true', () {
            final observableA = NonNullableObservableValuePatternMock('a');
            expect(observableA, observableA);
        });
        test('not equals value => false', () {
            final observableA = NonNullableObservableValuePatternMock('a');
            final observableB = NonNullableObservableValuePatternMock('b');
            expect(observableA == observableB, false);
        });
        test('not equals type => false', () {
            final observableA = NonNullableObservableValuePatternMock('a');
            final otherA = NonNullableObservableValuePatternMock2('a');
            expect(observableA.toString(), otherA.toString());
            expect(observableA == otherA, equals(false));
        });
    });

}