// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:test/test.dart';
import 'package:bpriver_origin/bpriver_origin.dart';

class EnumPatternMockA
    with
        EnumPattern
{
    static const String VALUE = 'a';
    @override
    String get value => VALUE;
}

/// == operator test 用。値が同じでも型が異なれば false を返すか test.
class EnumPatternMockA2
    with
        EnumPattern
{
    static const String VALUE = 'a';
    @override
    String get value => VALUE;
}

void main(){

    group('toString()', () {
        test('expected: return String value a', () {
            final base = EnumPatternMockA();
            final actual = base.toString();
            final expected = 'a';
            expect(actual, expected);
        });
    });

    group('properties', () {
        test('expected: return Map<String, Object> value {name: a}', () {
            final base = EnumPatternMockA();
            final actual = base.properties;
            final expected = {
                'value': 'a',
            };
            expect(actual, expected);
        });
    });

    group('toJson()', () {
        test('expected: return String value a', () {
            final base = EnumPatternMockA();
            final actual = base.toJson();
            final expected = 'a';
            expect(actual, expected);
        });
    });

}