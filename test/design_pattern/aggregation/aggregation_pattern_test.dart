// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:convert';

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';

class ValuePatternMock
    with
        ValuePattern<Object>
    implements
        InactivateSignature
{
    static const inactivateValue = 'xxx';
    ValuePatternMock(this.value);
    @override
    final Object value;
    @override
    Object inactivate() {
        return inactivateValue;
    }
}

class AggregationPatternMock
    with
        AggregationPattern
        ,AggregationPatternTester
{
    
    AggregationPatternMock(this.string, this.valuePatternMock, this.aggregationPatternMock2);
    
    final String string;
    final ValuePatternMock valuePatternMock;
    final AggregationPatternMock2 aggregationPatternMock2;
    
    @override
    Map<String,Object> get properties {
        return {
            'string': string,
            'valuePatternMock': valuePatternMock,
            'aggregationPatternMock2': aggregationPatternMock2,
        };
    }
    
}

class AggregationPatternMock2
    with
        AggregationPattern
        ,AggregationPatternTester
    implements
        AggregationPatternToShorthandSignature
{
    AggregationPatternMock2(this.string);
    final String string;
    @override
    Map<String,Object> get properties {
        return {
            'string': string,
        };
    }
    
    @override
    String toShorthand() => string;
    
}

class AggregationPatternMock3
    with
        AggregationPattern
        ,AggregationPatternTester
{
    AggregationPatternMock3(this.string);
    final String string;
    @override
    Map<String,Object> get properties {
        return {
            'string': string,
        };
    }
}

class AggregationPatternMock4
    with
        AggregationPattern
        ,AggregationPatternTester
{
    AggregationPatternMock4(this.object);
    final Object object;
    @override
    Map<String,Object> get properties {
        return {
            'object': object,
        };
    }
}

class AggregationPatternMock5
    with
        AggregationPattern
        ,AggregationPatternTester
{
    final AggregationPatternMock aggregationPatternMock;
    AggregationPatternMock5(this.aggregationPatternMock);
    @override
    Map<String,Object> get properties {
        return {
            'aggregationPatternMock': aggregationPatternMock,
        };
    }
}

class AggregationPatternMock6
    with
        AggregationPattern
        ,AggregationPatternTester
{
    final AggregationPatternMock5 aggregationPatternMock5;
    AggregationPatternMock6(this.aggregationPatternMock5);
    @override
    Map<String,Object> get properties {
        return {
            'aggregationPatternMock5': aggregationPatternMock5,
        };
    }
}

// toJson の generics 有り時の test に必要.
class AggregationPatternMock7<T extends Object>
    with
        AggregationPattern
{
    AggregationPatternMock7(this.object);
    final T object;
    @override
    Map<String,Object> get properties {
        return {
            'object': object,
        };
    }
}

// toString が properties が空だった場合に期待通りに動くかの test に必要.
class AggregationPatternMock8
    with
        AggregationPattern
{
    AggregationPatternMock8();
    @override
    Map<String,Object> get properties {
        return {
        };
    }
}

class IterablePatternMock
    with
        ListPattern<ValuePatternMock>
{
    final Iterable<ValuePatternMock> values;
    IterablePatternMock(this.values);
}

class IterablePatternMock2
    with
        ListPattern<AggregationPatternMock2>
{
    final Iterable<AggregationPatternMock2> values;
    IterablePatternMock2(this.values);
}

void main() {

    final valueA = AggregationPatternMock(
        'a',
        ValuePatternMock('b'),
        AggregationPatternMock2('c'),
    );
    final valueA_otherInstance = AggregationPatternMock(
        'a',
        ValuePatternMock('b'),
        AggregationPatternMock2('c'),
    );
    final valueB = AggregationPatternMock(
        'c',
        ValuePatternMock('d'),
        AggregationPatternMock2('c'),
    );
    final valueC = AggregationPatternMock(
        'e',
        ValuePatternMock('f'),
        AggregationPatternMock2('c'),
    );
    final valueZ = AggregationPatternMock(
        'x',
        ValuePatternMock('y'),
        AggregationPatternMock2('c'),
    );

    group('get hashCode', () {
        test('value が同じなら、hashCode の値も変わらない', () {
            final baseA = AggregationPatternMock(
                'a',
                ValuePatternMock('b'),
                AggregationPatternMock2('c')
            );
            final baseB = AggregationPatternMock(
                'a',
                ValuePatternMock('b'),
                AggregationPatternMock2('c')
            );
            final actual = baseA.hashCode;
            final expected = baseB.hashCode;
            expect(actual, expected);
        });
        test('not equals', () {
            final baseA = AggregationPatternMock(
                'a',
                ValuePatternMock('b'),
                AggregationPatternMock2('c')
            );
            final baseB = AggregationPatternMock(
                'x',
                ValuePatternMock('b'),
                AggregationPatternMock2('c')
            );
            final actual = baseA.hashCode == baseB.hashCode;
            final expected = false;
            expect(actual, expected);
        });
    });

    group('operator ==', () {
        test('same type, other instance, same values => true', () {
            expect(valueA == valueA_otherInstance, isTrue);
        });
        test('other type, same values => false', () {
            final actual = AggregationPatternMock2('a') == AggregationPatternMock3('a');
            final expected = false;
            expect(actual, expected);
        });
        test('other', () {
            expect(valueB == valueB, isTrue);
        expect(valueC == valueC, isTrue);
            expect(valueZ == valueZ, isTrue);
            expect(valueB == valueC, equals(false));
            expect(valueB == valueZ, equals(false));
            expect(valueC == valueZ, equals(false));
        });
    });

    group('operator []', () {
        test('equals', () {
            expect(valueA['string'].toString(), equals(valueA.string.toString()));
            expect(valueA['valuePatternMock'].toString(), equals(valueA.valuePatternMock.toString()));
        });
        test('key として不適切 => throw AggregationPatternErrorA', () {
            expect(() => valueA['xxx'], throwsA(TypeMatcher<Panic<AggregationPatternErrorA>>()));
        });
    });

    group('toString()', () {
        test('equals', () {
            final base = AggregationPatternMock(
                'a',
                ValuePatternMock('b'),
                AggregationPatternMock2('c'),
            );
            final actual = base.toString();
            final expected = '{string: a, valuePatternMock: b, aggregationPatternMock2: {string: c}}';
            expect(actual, expected);
        });
        test('expected: return String then properties is empty', () {
            final base = AggregationPatternMock8();
            final result = base.toString();
            final actual = result;
            final expected = '{}';
            expect(actual, expected);
        });
    });

    group('toJson()', () {
        test('a', () {
            final base = AggregationPatternMock(
                'a',
                ValuePatternMock('b'),
                AggregationPatternMock2(
                    'c',
                ),
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock': {
                    'string': 'a',
                    'valuePatternMock': {
                        'ValuePatternMock': {
                            'value': 'b',
                        }
                    },
                    'aggregationPatternMock2': {
                        'AggregationPatternMock2': {
                            'string': 'c',
                        }
                    }
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is ValuePattern', () {
            final base = AggregationPatternMock4(
                ValuePatternMock('a'),
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': {
                        'ValuePatternMock': {
                            'value': 'a'
                        }
                    },
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is AggregationPattern', () {
            final base = AggregationPatternMock4(
                AggregationPatternMock(
                    'a',
                    ValuePatternMock('b'),
                    AggregationPatternMock2('c'),
                ),
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': {
                        'AggregationPatternMock': {
                            'string': 'a',
                            'valuePatternMock': {
                                'ValuePatternMock': {
                                    'value': 'b',
                                }
                            },
                            'aggregationPatternMock2': {
                                'AggregationPatternMock2': {
                                    'string': 'c',
                                }
                            },
                        }
                    },
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is IterablePattern', () {
            final base = AggregationPatternMock4(
                IterablePatternMock([
                    ValuePatternMock('a'),
                    ValuePatternMock('b'),
                    ValuePatternMock('c'),
                ]),
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': {
                        'IterablePatternMock': {
                            'values': [
                                {'ValuePatternMock': {'value': 'a'}},
                                {'ValuePatternMock': {'value': 'b'}},
                                {'ValuePatternMock': {'value': 'c'}},
                            ]
                        }
                    },
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is int', () {
            final base = AggregationPatternMock4(
                1,
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': 1,
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is double', () {
            final base = AggregationPatternMock4(
                0.1,
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': 0.1,
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is bool', () {
            final base = AggregationPatternMock4(
                true,
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': true,
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is List<String>', () {
            final base = AggregationPatternMock4(
                <String>['a', 'b', 'c'],
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': ['a', 'b', 'c'],
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then object is List<int>', () {
            final base = AggregationPatternMock4(
                <int>[1, 2, 3],
            );
            final actual = base.toJson();
            final expected = <String,Object>{
                'AggregationPatternMock4': {
                    'object': [1, 2, 3],
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('then generics', () {
            final base = AggregationPatternMock7<String>('a');
            final actual = base.toJson();
            final expected = {
                'AggregationPatternMock7': {
                    'object': 'a',
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
    });

    group('inactivate()', () {
        test('properties contains value pattern and aggregation pattern.', () {
            final base = AggregationPatternMock(
                'a',
                ValuePatternMock('b'),
                AggregationPatternMock2(
                    'c',
                ),
            );
            final actual = base.inactivate();
            final expected = <String,Object>{
                'string': 'a',
                'valuePatternMock': 'xxx',
                'aggregationPatternMock2': {
                    'string': 'c',
                }
            };
            expect(actual, expected);
        });
        test('properties contains iterable pattern.', () {
            final base = AggregationPatternMock4(
                IterablePatternMock([
                    ValuePatternMock('a'),
                    ValuePatternMock('b'),
                    ValuePatternMock('c'),
                ])
            );
            final actual = base.inactivate();
            final expected = <String,Object>{
                'object': ['xxx', 'xxx', 'xxx'],
            };
            expect(actual, expected);
        });
    });

    group('_toNestedStructureMapEntryA', () {
        test('expected: return entry.toShorthand result then entryValue is AggregationPatternToShorthandSignature', () {
            final entryValue = AggregationPatternMock2('aaa');
            final base = AggregationPatternMock(
                'bbb',
                ValuePatternMock('ccc'),
                AggregationPatternMock2('ddd'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryA(entryValue);
            final expected = 'aaa';
            expect(actual, expected);
        });
        test('expected: return entry.toNestedStructureMapentry().value result then entryValue is DesignPatternToNestedStructureMapEntrySignature', () {
            final entryValue = AggregationPatternMock(
                'aaa',
                ValuePatternMock('bbb'),
                AggregationPatternMock2('ccc'),
            );
            final base = AggregationPatternMock(
                'ddd',
                ValuePatternMock('eee'),
                AggregationPatternMock2('fff'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryA(entryValue);
            final expected = {
                'String(string)': 'aaa',
                'ValuePatternMock(valuePatternMock)': 'bbb',
                'AggregationPatternMock2(aggregationPatternMock2)': 'ccc',
            };
            expect(actual, expected);
        });
        test('expected: return entry.toNestedStructureMapentry().value result then entryValue is IterablePattern<ValuePattern>', () {
            final entryValue = IterablePatternMock([
                ValuePatternMock('aaa'),
                ValuePatternMock('bbb'),
                ValuePatternMock('ccc'),
            ]);
            final base = AggregationPatternMock(
                'ddd',
                ValuePatternMock('eee'),
                AggregationPatternMock2('fff'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryA(entryValue);
            final expected = entryValue.toNestedStructureMapEntry().value;
            expect(actual, expected);
        });
        test('expected: return entry.toNestedStructureMapentry().value result then entryValue is IterablePattern<AggregationPattern>', () {
            final entryValue = IterablePatternMock2([
                AggregationPatternMock2('aaa'),
                AggregationPatternMock2('bbb'),
                AggregationPatternMock2('ccc'),
            ]);
            final base = AggregationPatternMock(
                'ddd',
                ValuePatternMock('eee'),
                AggregationPatternMock2('fff'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryA(entryValue);
            final expected = entryValue.toNestedStructureMapEntry().value;
            expect(actual, expected);
        });
        test('expected: return entry.value result then entryValue is ValuePattern', () {
            final entryValue = ValuePatternMock(
                'aaa',
            );
            final base = AggregationPatternMock(
                'bbb',
                ValuePatternMock('ccc'),
                AggregationPatternMock2('ddd'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryA(entryValue);
            final expected = 'aaa';
            expect(actual, expected);
        });
        test('expected: return entry.toString() result then entryValue is other', () {
            final entryValue = 100;
            final base = AggregationPatternMock(
                'aaa',
                ValuePatternMock('bbb'),
                AggregationPatternMock2('ccc'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryA(entryValue);
            final expected = '100';
            expect(actual, expected);
        });
    });

    group('_toNestedStructureMapEntryB', () {
        test('expected: return Map<String, Object>', () {
            final self = AggregationPatternMock(
                'aaa',
                ValuePatternMock('bbb'),
                AggregationPatternMock2('ccc'),
            );
            final base = AggregationPatternMock(
                'ddd',
                ValuePatternMock('eee'),
                AggregationPatternMock2('fff'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryB(self);
            final expected = {
                'String(string)': 'aaa',
                'ValuePatternMock(valuePatternMock)': 'bbb',
                'AggregationPatternMock2(aggregationPatternMock2)': 'ccc',
            };
            expect(actual, expected);
        });
    });

    group('_toNestedStructureMapEntryC', () {
        test('expected: return self.toShorthand() then self is AggregationPatternToShorthandSignature', () {
            final self = AggregationPatternMock2(
                'aaa',
            );
            final base = AggregationPatternMock(
                'bbb',
                ValuePatternMock('ccc'),
                AggregationPatternMock2('ddd'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryC(self);
            final expected = self.toShorthand();
            expect(actual, expected);
        });
        test('expected: return self.toNestedStructureMapEntryB() then self is other(AggregationPattern)', () {
            final self = AggregationPatternMock(
                'aaa',
                ValuePatternMock('bbb'),
                AggregationPatternMock2('ccc'),
            );
            final base = AggregationPatternMock(
                'ddd',
                ValuePatternMock('eee'),
                AggregationPatternMock2('fff'),
            );
            // final result = ;
            final actual = base.toNestedStructureMapEntryC(self);
            final expected = self.toNestedStructureMapEntryB(self);
            expect(actual, expected);
        });
    });

    // toNestedStructureMap は toNestedStructureMapEntry 結果を Map Type にしただけのもの.
    // 等価比較の際 MapEntry ではできず Map じゃないとできない.
    // なので toNestedStructureMap を等価比較することで toNestedStructureMapEntry test も兼ねる.
    group('toNestedStructureMapEntry and toNestedStructureMap', () {
        test('expected: return MapEntry<String, Object> then depth 2', () {
            final base = AggregationPatternMock(
                'aaa',
                ValuePatternMock('bbb'),
                AggregationPatternMock2('ccc'),
            );
            // final result = ;
            final actual = base.toNestedStructureMap();
            final expected = {
                'AggregationPatternMock': {
                    'String(string)': 'aaa',
                    'ValuePatternMock(valuePatternMock)': 'bbb',
                    'AggregationPatternMock2(aggregationPatternMock2)': 'ccc',
                }
            };
            expect(actual, expected);
        });
        test('expected: return MapEntry<String, Object> then depth 4', () {
            final base = AggregationPatternMock6(
                AggregationPatternMock5(
                    AggregationPatternMock(
                        'aaa',
                        ValuePatternMock('bbb'),
                        AggregationPatternMock2('ccc'),
                    ),
                )
            );
            // final result = ;
            final actual = base.toNestedStructureMap();
            final expected = {
                'AggregationPatternMock6': {
                    'AggregationPatternMock5(aggregationPatternMock5)': {
                        'AggregationPatternMock(aggregationPatternMock)': {
                            'String(string)': 'aaa',
                            'ValuePatternMock(valuePatternMock)': 'bbb',
                            'AggregationPatternMock2(aggregationPatternMock2)': 'ccc',
                        }
                    },
                },
            };
            expect(actual, expected);
        });
    });

}
