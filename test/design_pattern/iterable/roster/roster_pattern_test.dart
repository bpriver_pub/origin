// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';

class ValuePatternMock
    with
        ValuePattern<String>
{
    final String value;
    ValuePatternMock(this.value);
}

class EntityPatternMock
    with
        AggregationPattern
        ,EntityPattern<ValuePatternMock>
{
    final ValuePatternMock valuePatternMock;
    EntityPatternMock(this.valuePatternMock);
    @override
    ValuePatternMock get primaryKey => valuePatternMock;
    @override
    Map<String, Object> get properties {
        return {
            'valuePatternMock': valuePatternMock,
        };
    }
}

class RosterPatternMock
    extends
        Computation
    with
        RosterPattern<
            ValuePatternMock
            ,EntityPatternMock
        >
{
    final Iterable<EntityPatternMock> values;
    RosterPatternMock(this.values);
}

void main() {

    final valuePatternMockA = ValuePatternMock('a');
    final valuePatternMockB = ValuePatternMock('b');
    final valuePatternMockC = ValuePatternMock('c');
    final entityPatternMockA = EntityPatternMock(valuePatternMockA);
    final entityPatternMockB = EntityPatternMock(valuePatternMockB);
    final entityPatternMockC = EntityPatternMock(valuePatternMockC);

    final values = [entityPatternMockA, entityPatternMockB, entityPatternMockC];

    group('generative constructor', () {
        test('expected: return RosterPatternMock then not duplicate', () {
            final base = RosterPatternMock(values);
            final result = base;
            final actual = result;
            final expected = RosterPatternMock(values);
            expect(actual, expected);
        });
        test('exception: return RosterPatternExceptionA then duplicate', () {
            try {
                RosterPatternMock([entityPatternMockA, entityPatternMockA]);
            } on Failure<Object, RosterPatternExceptionA> catch (e) {
                final actual = e.log.getMonitor();
                final expected = {
                    'primary keys': [valuePatternMockA, valuePatternMockA],
                };
                expect(actual, expected);                
            }
        });
    });

    group('getPrimaryKey()', () {
        final base = RosterPatternMock(values);
        test('expected: return VT then 指定した primaryKey が存在した', () {
            final result = base.getByPrimaryKey(valuePatternMockA);
            final actual = result.wrapped;
            final expected = entityPatternMockA;
            expect(actual, expected);
        });
        test('exception: return RosterPatternExceptionB then 指定した primaryKey の object が存在しない', () {
            final primaryKey = ValuePatternMock('xxxxx');
            final result = base.getByPrimaryKey(primaryKey);
            final actual = result.wrapped;
            final expected = RosterPatternExceptionB();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'primary key': primaryKey,
                    'primary keys': [
                        entityPatternMockA.primaryKey,
                        entityPatternMockB.primaryKey,
                        entityPatternMockC.primaryKey,
                    ],
                };
                expect(actual, expected);
            }
        });
    });

    group('containsPrimaryKey', () {
        final valuePatternMockA = ValuePatternMock('a');
        final valuePatternMockB = ValuePatternMock('b');
        final valuePatternMockC = ValuePatternMock('c');
        final entityPatternMockA = EntityPatternMock(valuePatternMockA);
        final entityPatternMockB = EntityPatternMock(valuePatternMockB);
        final entityPatternMockC = EntityPatternMock(valuePatternMockC);
        test('expected: return true then exists primary key', () {
            final base = RosterPatternMock([
                entityPatternMockA,
                entityPatternMockB,
                entityPatternMockC,
            ]);
            final result = base.containsPrimaryKey(valuePatternMockA);
            final actual = result.wrapped;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return false then not exists primary key', () {
            final base = RosterPatternMock([
                entityPatternMockB,
                entityPatternMockC,
            ]);
            final result = base.containsPrimaryKey(valuePatternMockA);
            final actual = result.wrapped;
            final expected = false;
            expect(actual, expected);
        });
        test('expected: return false then values is empty', () {
            final base = RosterPatternMock([]);
            final result = base.containsPrimaryKey(valuePatternMockA);
            final actual = result.wrapped;
            final expected = false;
            expect(actual, expected);
        });
    });

    group('containsPrimaryKeyAll', () {
        test('expected: return true then contain all primary key ', () {
            final primaryKeys = [
                entityPatternMockA.primaryKey,
                entityPatternMockB.primaryKey,
                entityPatternMockC.primaryKey,
            ];
            final base = RosterPatternMock(values);
            final result = base.containsPrimaryKeyAll(primaryKeys);
            final actual = result.wrapped;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return true then primaryKeys is empty', () {
            final primaryKeys = <ValuePatternMock>[];
            final base = RosterPatternMock(values);
            final result = base.containsPrimaryKeyAll(primaryKeys);
            final actual = result.wrapped;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return true then primaryKeys and values is empty', () {
            final primaryKeys = <ValuePatternMock>[];
            final base = RosterPatternMock([]);
            final result = base.containsPrimaryKeyAll(primaryKeys);
            final actual = result.wrapped;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return false then not contain primary key ', () {
            final primaryKeys = [
                entityPatternMockA.primaryKey,
                ValuePatternMock('dummy'),
            ];
            final base = RosterPatternMock(values);
            final result = base.containsPrimaryKeyAll(primaryKeys);
            final actual = result.wrapped;
            final expected = false;
            expect(actual, expected);
        });
        test('expected: return false then values is empty ', () {
            final primaryKeys = [
                entityPatternMockA.primaryKey,
            ];
            final base = RosterPatternMock([]);
            final result = base.containsPrimaryKeyAll(primaryKeys);
            final actual = result.wrapped;
            final expected = false;
            expect(actual, expected);
        });

    });

    group('primaryKeys', () {
        test('expected: return List<PK> then ', () {
            final base = RosterPatternMock(values);
            final result = base.primaryKeys;
            final actual = result;
            final expected = [
                entityPatternMockA.primaryKey,
                entityPatternMockB.primaryKey,
                entityPatternMockC.primaryKey,
            ];
            expect(actual, expected);
        });
    });

}
