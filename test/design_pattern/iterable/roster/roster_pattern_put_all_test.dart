// Copyright (C) 2023, the bpriver_origin_extension project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';

class EntityPatternMock
    with
        AggregationPattern
        ,EntityPattern<String>
{
    @override
    final String primaryKey;
    final String other;

    EntityPatternMock(this.primaryKey, this.other);

    @override
    Map<String, Object> get properties {
        return {
            'primaryKey': primaryKey,
            'other': other,
        };
    }

}

class RosterPatternPutAllMock
    extends
        Computation
    with
        RosterPattern<String, EntityPatternMock>
        ,IterablePatternInternalFactory<EntityPatternMock, Safety<RosterPatternPutAllMock>>
        ,RosterPatternPutAll<String, EntityPatternMock, Safety<RosterPatternPutAllMock>>
{

    @override
    final Iterable<EntityPatternMock> values;
    
    RosterPatternPutAllMock(this.values);

    static Safety<RosterPatternPutAllMock> result(Iterable<EntityPatternMock> values) {
        return Safety(RosterPatternPutAllMock(values));
    }

    Safety<RosterPatternPutAllMock> internalFactory(Iterable<EntityPatternMock> values) => result(values);

}


void main() {

    final valueA = EntityPatternMock('a', 'aaa');
    final valueB = EntityPatternMock('b', 'bbb');
    final valueC = EntityPatternMock('c', 'ccc');

    final values = [valueA, valueB, valueC];

    group('putAll', () {
        test('expected: return RosterPatternPutAllMock then already exists duplicate primary key', () {
            final putValues = [
                EntityPatternMock('a', 'ddd'),
                EntityPatternMock('b', 'eee'),
            ];
            final base = RosterPatternPutAllMock(values);
            final result = base.putAll(putValues);
            final actual = result.wrapped;
            final expected = RosterPatternPutAllMock([
                EntityPatternMock('a', 'ddd'),
                EntityPatternMock('b', 'eee'),
                valueC,
            ]);
            expect(actual, expected);
        });
        test('expected: return RosterPatternPutAllMock then not exists duplicate primary key', () {
            final putValues = [
                EntityPatternMock('d', 'ddd'),
                EntityPatternMock('e', 'eee'),
            ];
            final base = RosterPatternPutAllMock(values);
            final result = base.putAll(putValues);
            final actual = result.wrapped;
            final expected = RosterPatternPutAllMock([
                valueA,
                valueB,
                valueC,
                EntityPatternMock('d', 'ddd'),
                EntityPatternMock('e', 'eee'),
            ]);
            expect(actual, expected);
        });
        test('expected: return RosterPatternPutAllMock then compound', () {
            final putValues = [
                EntityPatternMock('a', 'new value'),
                EntityPatternMock('d', 'ddd'),
            ];
            final base = RosterPatternPutAllMock(values);
            final result = base.putAll(putValues);
            final actual = result.wrapped;
            final expected = RosterPatternPutAllMock([
                EntityPatternMock('a', 'new value'),
                valueB,
                valueC,
                EntityPatternMock('d', 'ddd'),
            ]);
            expect(actual, expected);
        });
    });

}
