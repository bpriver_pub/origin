// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:convert';

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';

class IterablePatternMock
    with
        ListPattern<Object>
        ,ListPatternPrivateMemberTester<Object>
{
    IterablePatternMock(this.values);
    final Iterable<Object> values;
}

class IterablePatternMock2
    with
        ListPattern<Object>
        ,ListPatternPrivateMemberTester<Object>
{
    IterablePatternMock2(this.values);
    final Iterable<Object> values;
}

class ValuePatternMock
    with
        ValuePattern<Object>
    implements
        InactivateSignature
{
    ValuePatternMock(this.value);
    final Object value;
    @override
    Object inactivate() {
        return InactivateValueMock.xxxxx;
    }
}

class IterablePatternMock3
    with
        ListPattern<ValuePatternMock>
        ,ListPatternPrivateMemberTester<ValuePatternMock>
{
    IterablePatternMock3(this.values);
    final Iterable<ValuePatternMock> values;
}

class AggregationPatternMock
    with
        AggregationPattern
        ,AggregationPatternToShorthandSignature
{
    AggregationPatternMock(this.valuePatternMock);
    final ValuePatternMock valuePatternMock;
    @override
    Map<String, Object> get properties {
        return {
            'valuePatternMock': valuePatternMock,
        };
    }
    
    @override
    String toShorthand() => valuePatternMock.value.toString();

}

// AggregationPatternToShorthandSignature を implements してはいけない. それ用に用意したので.
class AggregationPatternMock2
    with
        AggregationPattern
{
    AggregationPatternMock2(this.valuePatternMock1, this.valuePatternMock2, this.valuePatternMock3);
    final ValuePatternMock valuePatternMock1;
    final ValuePatternMock valuePatternMock2;
    final ValuePatternMock valuePatternMock3;
    @override
    Map<String, Object> get properties {
        return {
            'valuePatternMock1': valuePatternMock1,
            'valuePatternMock2': valuePatternMock2,
            'valuePatternMock3': valuePatternMock3,
        };
    }
}

class IterablePatternMock4
    with
        ListPattern<AggregationPatternMock>
        ,ListPatternPrivateMemberTester<AggregationPatternMock>
{
    IterablePatternMock4(this.values);
    final Iterable<AggregationPatternMock> values;
}

class InactivateValueMock
    extends
        Thin
{
    static const String xxxxx = 'xxxxx';
}

// toJson generics
class IterablePatternMock5<T extends Object>
    with
        ListPattern<T>
{
    IterablePatternMock5(this.values);
    final Iterable<T> values;
}

void main() {

    group('get hashCode', () {
        test('value が同じなら、hashCode の値も変わらない', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final otherInstance = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final actual = base.hashCode;
            final expected = otherInstance.hashCode;
            expect(actual, expected);
        });
    });

    group('operator ==', () {
        test('[] == [] => true', () {
            final base = IterablePatternMock([]);
            final actual = base;
            final expected = IterablePatternMock([]);
            expect(actual, expected);
        });
        test('[A,B,C] == [A,B,C] => true', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final actual = base;
            final expected = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            expect(actual, expected);
        });
        test('[A,A,A] == [A,A] => false', () {
            final base = IterablePatternMock([
                'a',
                'a',
                'a',
            ]);
            final base2 = IterablePatternMock([
                'a',
                'a',
            ]);
            final actual = base == base2;
            final expected = false;
            expect(actual, expected);
        });
        test('[A,A] [A,B] => false', () {
            final base = IterablePatternMock([
                'a',
                'a',
            ]);
            final base2 = IterablePatternMock([
                'a',
                'b',
            ]);
            final actual = base == base2;
            final expected = false;
            expect(actual, expected);
        });
        test('other type => false', () {
            final base = IterablePatternMock([
                'a',
            ]);
            final base2 = IterablePatternMock2([
                'a',
            ]);
            final actual = base == base2;
            final expected = false;
            expect(actual, expected);
        });
        test('return false then 中身が同じでも 順番が異なる.', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final other = IterablePatternMock([
                'b',
                'a',
                'c',
            ]); 
            final actual = base == other;
            final expected = false;
            expect(actual, expected);
        });
    });

    group('containsAll()', () {
        test('true 1', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final base2 = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final actual = base.containsAll(base2);
            final expected = true;
            expect(actual, expected);
        });
        test('true 2', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final base2 = IterablePatternMock([
                'b',
                'c',
            ]);
            final actual = base.containsAll(base2);
            final expected = true;
            expect(actual, expected);
        });
        test('false', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final base2 = IterablePatternMock([
                'b',
                'c',
                'z',
            ]);
            final actual = base.containsAll(base2);
            final expected = false;
            expect(actual, expected);
        });
    });

    group('properties', () {
        test('', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final actual = base.properties;
            final expected = {
                'values': [
                    'a',
                    'b',
                    'c',
                ]
            };
            expect(actual, expected);
        });
    });

    group('toJson()', () {
        test('then VT type is BuiltInType', () {
            final base = IterablePatternMock([
                'a',
                'b',
                'c',
            ]);
            final actual = base.toJson();
            final expected = {
                'IterablePatternMock': {
                    'values': [
                        'a',
                        'b',
                        'c',
                    ]
                }
            };
            jsonEncode(actual);
            expect(actual, expected);
        });
        test('if VT type is value pattern then', () {
            final actual = IterablePatternMock3([
                ValuePatternMock('a'),
            ]).toJson();
            final expected = {
                'IterablePatternMock3': {
                    'values': [
                        {'ValuePatternMock': {'value': 'a'}},
                    ],
                }
            };
            expect(actual, expected);
        });
        test('if VT type is aggregation pattern then', () {
            final actual = IterablePatternMock4([
                AggregationPatternMock(ValuePatternMock('a')),
            ]).toJson();
            final expected = {
                'IterablePatternMock4': {
                    'values': [
                        {
                            'AggregationPatternMock': {
                                'valuePatternMock': {
                                    'ValuePatternMock': {
                                        'value': 'a'
                                    }
                                }
                            }
                        },
                    ],
                }
            };
            expect(actual, expected);
        });
        test('if VT type is other then', () {
            final actual = IterablePatternMock([
                'a',
            ]).toJson().toString();
            final expected = {
                'IterablePatternMock': {
                    'values': [
                        'a',
                    ],
                }
            }.toString();
            expect(actual, expected);
        });
        test('then generics', () {
            final actual = IterablePatternMock5<String>([
                'a',
            ]).toJson().toString();
            final expected = {
                'IterablePatternMock5': {
                    'values': [
                        'a',
                    ],
                }
            }.toString();
            expect(actual, expected);
        });
        test('then values value is class', () {
            final actual = IterablePatternMock(IterablePatternMock([
                'a',
                'b',
                'c',
            ])).toJson().toString();
            final expected = {
                'IterablePatternMock': {
                    'values': [
                        'a',
                        'b',
                        'c',
                    ],
                }
            }.toString();
            expect(actual, expected);
        });
    });

    group('inactivate()', () {
        test('if VT type is value pattern then', () {
            final actual = IterablePatternMock3([
                ValuePatternMock('a'),
            ]).inactivate();
            final expected = [InactivateValueMock.xxxxx];
            expect(actual, expected);
        });
        test('if VT type is aggregation pattern then', () {
            final actual = IterablePatternMock4([
                AggregationPatternMock(ValuePatternMock('a')),
            ]).inactivate();
            final expected = [
                { 'valuePatternMock': InactivateValueMock.xxxxx},
            ];
            expect(actual, expected);
        });
        test('if VT type is other then', () {
            final actual = IterablePatternMock([
                'a',
            ]).inactivate().toString();
            final expected = [
                'a',
            ].toString();
            expect(actual, expected);
        });
    });

    group('toSet()', () {
        group('value pattern', () {
        test('all different value then return all', () {
            final base = IterablePatternMock3([
                ValuePatternMock('a'),
                ValuePatternMock('b'),
                ValuePatternMock('c'),
            ]);
            final actual = base.toSet();
            final expected = {
                ValuePatternMock('a'),
                ValuePatternMock('b'),
                ValuePatternMock('c'),
            };
            expect(actual, expected);
        });
        test('exists a different value then pattern a', () {
            final base = [
                ValuePatternMock('a'),
                ValuePatternMock('a'),
                ValuePatternMock('c'),
            ];
            final actual = base.toSet();
            final expected = {
                ValuePatternMock('a'),
                ValuePatternMock('c'),
            };
            expect(actual, expected);
        });
        test('exists a different value then pattern b', () {
            final base = [
                ValuePatternMock('a'),
                ValuePatternMock('a'),
                ValuePatternMock('a'),
            ];
            final actual = base.toSet();
            final expected = {
                ValuePatternMock('a'),
            };
            expect(actual, expected);
        });
        test('exists a different value then pattern c', () {
            final base = [
                ValuePatternMock('a'),
                ValuePatternMock('a'),
                ValuePatternMock('b'),
                ValuePatternMock('b'),
                ValuePatternMock('c'),
                ValuePatternMock('c'),
            ];
            final actual = base.toSet();
            final expected = {
                ValuePatternMock('a'),
                ValuePatternMock('b'),
                ValuePatternMock('c'),
            };
            expect(actual, expected);
        });
        });

        group('aggregation pattern', () {
            test('all different value then return all', () {
                final base = IterablePatternMock4([
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('b')),
                    AggregationPatternMock(ValuePatternMock('c')),
                ]);
                final actual = base.toSet();
                final expected = {
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('b')),
                    AggregationPatternMock(ValuePatternMock('c')),
                };
                expect(actual, expected);
            });
            test('exists a different value then pattern a', () {
                final base = IterablePatternMock4([
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('c')),
                ]);
                final actual = base.toSet();
                final expected = {
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('c')),
                };
                expect(actual, expected);
            });
            test('exists a different value then pattern b', () {
                final base = [
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('a')),
                ];
                final actual = base.toSet();
                final expected = {
                    AggregationPatternMock(ValuePatternMock('a')),
                };
                expect(actual, expected);
            });
            test('exists a different value then pattern c', () {
                final base = [
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('b')),
                    AggregationPatternMock(ValuePatternMock('b')),
                    AggregationPatternMock(ValuePatternMock('c')),
                    AggregationPatternMock(ValuePatternMock('c')),
                ];
                final actual = base.toSet();
                final expected = {
                    AggregationPatternMock(ValuePatternMock('a')),
                    AggregationPatternMock(ValuePatternMock('b')),
                    AggregationPatternMock(ValuePatternMock('c')),
                };
                expect(actual, expected);
            });
        });
    });

    group('_toNestedStructureMapEntryA', () {
        test('expected: return element.toShorthand() then element is AggregationPatternToShorthandSignature', () {
            final element = AggregationPatternMock(
                ValuePatternMock('aaa')
            );
            final base = IterablePatternMock([]);
            final actual = base.toNestedStructureMapEntryA(element);
            final expected = element.toShorthand();
            expect(actual, expected);
        });
        test('expected: return toNestedStructureMapEntry().value then element is other', () {
            final element = AggregationPatternMock2(
                ValuePatternMock('aaa'),
                ValuePatternMock('bbb'),
                ValuePatternMock('ccc'),
            );
            final base = IterablePatternMock([]);
            final actual = base.toNestedStructureMapEntryA(element);
            final expected = element.toNestedStructureMapEntry().value;
            expect(actual, expected);
        });
    });

    group('_toNestedStructureMapEntryB', () {
        test('expected: return Map<String, Object>', () {
            final values = IterablePatternMock4([
                AggregationPatternMock(ValuePatternMock('aaa')),
                AggregationPatternMock(ValuePatternMock('bbb')),
                AggregationPatternMock(ValuePatternMock('ccc')),
            ]);
            final base = IterablePatternMock([]);
            final actual = base.toNestedStructureMapEntryB(values);
            final expected = {
                'AggregationPatternMock[0]': 'aaa',
                'AggregationPatternMock[1]': 'bbb',
                'AggregationPatternMock[2]': 'ccc',
            };
            expect(actual, expected);
        });
    });

    group('_toNestedStructureMapEntryC', () {
        test('expected: return this._toNestedStructureMapEntryB() then this is Iterable<AggregationPattern>', () {
            final base = IterablePatternMock4([
                AggregationPatternMock(ValuePatternMock('aaa')),
                AggregationPatternMock(ValuePatternMock('bbb')),
                AggregationPatternMock(ValuePatternMock('ccc')),
            ]);
            final actual = base.toNestedStructureMapEntryC();
            final expected = base.toNestedStructureMapEntryB(base);
            expect(actual, expected);
        });
        test('expected: return String then this is Iterable<ValuePattern>', () {
            final base = IterablePatternMock3([
                ValuePatternMock('aaa'),
                ValuePatternMock('bbb'),
                ValuePatternMock('ccc'),
            ]);
            final actual = base.toNestedStructureMapEntryC();
            final expected = 'aaa, bbb, ccc';
            expect(actual, expected);
        });
        test('expected: return this.toString() then this is other type', () {
            final base = IterablePatternMock([
                'aaa',
                'bbb',
                'ccc',
            ]);
            final actual = base.toNestedStructureMapEntryC();
            final expected = base.toString();
            expect(actual, expected);
        });
    });

    // Map じゃないと 等価比較ができないため MapEntry と兼ねる.
    group('toNestedStructureMapEntry and toNestedStructureMap', () {
        test('expected: return Map<String, Object>', () {
            final base = IterablePatternMock4([
                AggregationPatternMock(ValuePatternMock('aaa')),
                AggregationPatternMock(ValuePatternMock('bbb')),
                AggregationPatternMock(ValuePatternMock('ccc')),
            ]);
            final actual = base.toNestedStructureMap();
            final expected = {
                'IterablePatternMock4': {
                    'AggregationPatternMock[0]': 'aaa',
                    'AggregationPatternMock[1]': 'bbb',
                    'AggregationPatternMock[2]': 'ccc',
                },
            };
            expect(actual, expected);
        });
    });

    group('getByIndex', () {
        final values = [
            AggregationPatternMock(ValuePatternMock('aaa')),
            AggregationPatternMock(ValuePatternMock('bbb')),
            AggregationPatternMock(ValuePatternMock('ccc')),
        ];
        test('expected: return AggregationPatternMock then index is 0', () {
            final index = 0;
            final base = IterablePatternMock4(values);
            final result = base.getByIndex(index);
            final actual = result.wrapped;
            final expected = AggregationPatternMock(ValuePatternMock('aaa'));
            expect(actual, expected);
        });
        test('expected: return AggregationPatternMock then index is 2', () {
            final index = 2;
            final base = IterablePatternMock4(values);
            final result = base.getByIndex(index);
            final actual = result.wrapped;
            final expected = AggregationPatternMock(ValuePatternMock('ccc'));
            expect(actual, expected);
        });
        test('exception: return IterablePatternExceptionA then index is -1', () {
            final index = -1;
            final base = IterablePatternMock4(values);
            final result = base.getByIndex(index);
            final actual = result.wrapped;
            final expected = IterablePatternExceptionA();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'index': index,
                };
                expect(actual, expected);
            }
        });
        test('exception: return IterablePatternExceptionB then index is over length', () {
            final index = 3;
            final base = IterablePatternMock4(values);
            final result = base.getByIndex(index);
            final actual = result.wrapped;
            final expected = IterablePatternExceptionB();
            expect(actual, expected);
            {
                final actual = result.log.getMonitor();
                final expected = {
                    'index': index,
                    'length': base.length,
                };
                expect(actual, expected);
            }
        });
        test('exception: return AggregationPatternMock then index is 0 and length is 0', () {
            final index = 0;
            final base = IterablePatternMock4([]);
            final result = base.getByIndex(index);
            final actual = result.wrapped;
            final expected = IterablePatternExceptionC();
            expect(actual, expected);
        });
    });
    

}
