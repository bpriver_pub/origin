// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';

void main() {

    group('==', () {
        test('expected: return true then both loggerResultMessage is empty', () {
            final other = Complete();
            final base = Complete();
            final actual = base == other;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return true then both loggerResultMessage is same', () {
            final loggerResultMessage = [
                'a',
            ];
            final other = Complete(loggerResultMessage);
            final base = Complete(loggerResultMessage);
            final actual = base == other;
            final expected = true;
            expect(actual, expected);
        });
        test('expected: return false then both loggerResultMessage is different', () {
            final other = Complete(['a']);
            final base = Complete(['x']);
            final actual = base == other;
            final expected = false;
            expect(actual, expected);
        });
    });

}