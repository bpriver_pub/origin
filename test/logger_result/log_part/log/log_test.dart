// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';

class DummyA {}
class DummyB {}

void main() {

    group('generative constructor', () {
        test('expected: return Log bind test', () {
            final monitor = {
                'keyA': 'valueA',
            };
            final debug = {
                'keyB': 'valueB'
            };
            final base = Log(
                monitor: monitor,
                debug: debug,
            );
            final result = base;
            final actualA = result.getMonitor();
            final expectedA = monitor;
            final actualB = result.getDebug();
            final expectedB = debug;
            expect(actualA, expectedA);
            expect(actualB, expectedB);
        });
    });

    group('empty', () {
        test('expected: return empty Log ', () {
            final base = Log.empty();
            final result = base;
            final actual = result;
            final expected = Log(
                classLocation: NotExist,
                functionLocation: '',
                monitor: {},
                debug: {},
                historyList: [],
            );
            expect(actual, expected);
        });
    });

    group('getMonitor', () {
        test('expected: return Map<String, dynamic> then debug と bind を間違えてないか.', () {
            final base = Log(
                monitor: {
                    'keyA': 'valueA',
                }
            );
            final result = base;
            final actual = result.getMonitor();
            final expected = {
                'keyA': 'valueA',
            };
            expect(actual, expected);
        });
        test('expected: return Map<String, dynamic> then 別の instance になっているか確認.', () {
            final base = Log(
                monitor: {
                    'keyA': 'valueA',
                }
            );
            final monitorResult = base.getMonitor();
            monitorResult.addAll({
                'keyB': 'valueB',
            });
            final result = base;
            final actual = result.getMonitor();
            final expected = {
                'keyA': 'valueA',
            };
            expect(actual, expected);
        });
    });

    group('getDebug', () {
        test('expected: return Map<String, dynamic> then monitor と bind を間違えてないか.', () {
            final base = Log(
                debug: {
                    'keyA': 'valueA',
                }
            );
            final result = base;
            final actual = result.getDebug();
            final expected = {
                'keyA': 'valueA',
            };
            expect(actual, expected);
        });
        test('expected: return Map<String, dynamic> then 別の instance になっているか確認.', () {
            final base = Log(
                debug: {
                    'keyA': 'valueA',
                }
            );
            final debugResult = base.getDebug();
            debugResult.addAll({
                'keyB': 'valueB',
            });
            final result = base;
            final actual = result.getDebug();
            final expected = {
                'keyA': 'valueA',
            };
            expect(actual, expected);
        });
    });

    group('getHistoryList', () {
        test('expected: return Map<String, dynamic> then 別の instance になっているか確認.', () {
            final history = History(
                Success<Object, Exception>,
                [],
                NotExist,
                'main',
                {},
                {},
                [],
            );
            final base = Log(
                historyList: [
                    history,
                ]
            );
            final historyListResult = base.getHistoryList();
            historyListResult.addAll([
                history,
            ]);
            final result = base;
            final actual = result.getHistoryList();
            final expected = [
                history,
            ];
            expect(actual, expected);
        });
    });

    group('monitor', () {
        test('expected: return void then ', () {
            final base = Log(
                monitor: {},
            );
            final key = 'keyA';
            final value = 'valueA';
            base.monitor({key: value});
            final actual = base.getMonitor();
            final expected = {
                key: value,
            };
            expect(actual, expected);
        });
    });

    group('debug', () {
        test('expected: return void then ', () {
            final base = Log(
                debug: {},
            );
            final key = 'keyA';
            final value = 'valueA';
            base.debug({key: value});
            final actual = base.getDebug();
            final expected = {
                key: value,
            };
            expect(actual, expected);
        });
    });

    group('add', () {
        test('expected: return void then ', () {
            final base = Log(
                historyList: [],
            );
            final log = Log.empty();
            final success = Success<String, Exception>('', log);
            base.add(success);
            // final result = base.addMonitor();
            final actual = base.getHistoryList();
            final expected = [
                History(
                    String,
                    [],
                    NotExist,
                    '',
                    {},
                    {},
                    [],
                ),
            ];
            expect(actual, expected);
        });
    });

}
