// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';

class DummyA {}

void main() {

    group('location', () {
        test('expected: return String then classLocation is not NotExist.', () {
            final base = Log(
                classLocation: DummyA,
                functionLocation: 'main'
            );
            final result = base.location;
            final actual = result;
            final expected = 'DummyA.main()';
            expect(actual, expected);
        });
        test('expected: return String then classLocation is NotExist.', () {
            final base = Log(
                classLocation: NotExist,
                functionLocation: 'main'
            );
            final result = base.location;
            final actual = result;
            final expected = 'main()';
            expect(actual, expected);
        });
    });

}