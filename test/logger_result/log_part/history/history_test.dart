// Copyright (C) 2023, the bpriver_origin project authors.
// Please see the AUTHORS file for details.
// All rights reserved.
// Use of this source code is governed by a GPL-style license that can be found in the LICENSE file.

import 'dart:convert';

import 'package:bpriver_origin/bpriver_origin.dart';
import 'package:test/test.dart';
import 'package:yaml_writer/yaml_writer.dart';

class LoggerResultMessageSignatureMock
    implements
        LoggerResultMessageSignature
{
    @override
    final List<String> loggerResultMessage;

    LoggerResultMessageSignatureMock(this.loggerResultMessage);

}

class ExceptionMockA implements Exception, LoggerResultMessageSignature {
    List<String> get loggerResultMessage => ['exception a'];
}

class MockA {
    Safety<Complete> methodA() {
        final log = Log(classLocation: runtimeType, functionLocation: 'methodA');
        log.monitor({'monitor key a': 'monitor value a'});
        log.debug({'debug key a': 'debug value a'});
        final methodBResult = methodB();
        log.add(methodBResult);
        final methodCResult = methodC();
        log.add(methodCResult);
        return Safety(Complete(['message a']), log);
    }
    Danger<String, ExceptionMockA> methodB() {
        final log = Log(classLocation: runtimeType, functionLocation: 'methodB');
        return Success('b', log);
    }
    Danger<int, ExceptionMockA> methodC() {
        final log = Log(classLocation: runtimeType, functionLocation: 'methodC');
        return Failure(ExceptionMockA(), log);
    }
}

void main() {

    group('fromResult', () {
        test('expected: return History then ', () {
            final loggerResult = MockA().methodA();
            final base = History.fromResult(loggerResult);
            final result = base;
            final actual = result;
            final expected = History(
                Complete,
                ['message a'],
                MockA,
                'methodA',
                {'monitor key a': 'monitor value a'},
                {'debug key a': 'debug value a'},
                [
                    History(
                        String,
                        [],
                        MockA,
                        'methodB',
                        {},
                        {},
                        [],
                    ),
                    History(
                        ExceptionMockA,
                        ['exception a'],
                        MockA,
                        'methodC',
                        {},
                        {},
                        [],
                    ),
                ]
            );
            expect(actual, expected);
        });
    });

    group('()', () {
        final YamlWriter yamlWriterA = YamlWriter(indentSize: 4, allowUnquotedStrings: true);
        test('', () {
            final loggerLoggerResultMessageSignatureMock = LoggerResultMessageSignatureMock([
                'this is message a',
                'this is message b',
                'this is message c',
            ]);
            final monitor = {
                'a': 0,
                'b': 0,
                'd': {
                    'a': 0,
                    'b': 0,
                },
                'e': '''
aaaaaaa
bbbbbbbbbb
''',
                'f': 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa0',
            };
            final debug = {
                'a': 1,
                'b': 1,
                'd': {
                    'a': 1,
                    'b': 1,
                }
            };
            ;
            final childAChildA = History(
                Success<Object, Exception>,
                [],
                String,
                'childAchildA',
                monitor,
                debug,
                [],
            );
            final childA = childAChildA.change(
                functionLocation: 'childA',
                historyList: [childAChildA]
            );
            final childB = Safety(
                loggerLoggerResultMessageSignatureMock,
                Log(
                    functionLocation: 'childB',
                    monitor: monitor,
                    debug: debug,
                    historyList: [],
                ),
            );
            final base = Safety<LoggerResultMessageSignatureMock>(
                loggerLoggerResultMessageSignatureMock,
                Log(
                    functionLocation: 'main',
                    monitor: monitor,
                    debug: debug,
                    historyList: [
                        childA,
                        childB.toHistory(),
                    ],
                )
            );

            final moldMap = base.toHistory().mold();
            final jsonString = jsonEncode(moldMap);
            final yamlString = yamlWriterA.write(moldMap);

            print(jsonString);
            print(yamlString);
            // expect(actual, expected);
        });
    });

}



